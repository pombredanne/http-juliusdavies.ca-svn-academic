package ca.juliusdavies.csc589d.jarchive;

import ca.phonelist.util.Bytes;
import ca.phonelist.util.Pad;
import ca.phonelist.util.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * @author Julius Davies
 * @since Apr 16, 2010
 */
public class Src extends Artifact<Src> {

    private ArrayList<Block>[] blockStructure;
    private Block topBlock;
    private String packageName;
    private String className;
    private boolean isInterface;
    private boolean isEnum;
    private int minLine;
    private int maxLine;
    private int methodCount;
    private int size;

    public Src(File f) throws IOException {
        this(
                f.getName(),
                f.getCanonicalPath(),
                f.lastModified(),
                new FileInputStream(f)
        );
    }

    public Src(
            String name, String path, long date, InputStream in
    ) throws IOException {
        super(name, path, date);
        try {
            this.srcBytes = Bytes.streamToBytes(in, false);
            this.size = srcBytes.length;
        } finally {
            Util.close(in);
        }

        String src = new String(srcBytes, "UTF-8");
        src = cleanCommentsAndLiterals(src);
        String s = src.trim();
        if ("".equals(s)) {
            isEnum = true; // to be ignored
            isInterface = true;
            return;
        }


        try {
            blockStructure = getBlockStructure(src);
        } catch (Throwable t) {
            // System.out.println(path + " +++++ERRROR++++(" + t + ")+++++");
            // System.out.println(src);
            throw new RuntimeException("Couldn't parse java source: " + t, t);
        }
        if (blockStructure[0].isEmpty()) {
            // System.out.println(path + " +++++ERRROR++++++(NO BLOCKS)++++");
            // System.out.println(src);
            throw new RuntimeException("Couldn't parse java source: no blocks");
        }
        topBlock = blockStructure[0].get(0);
        topBlock.init(null);
        minLine = topBlock.line;
        maxLine = topBlock.line;
        this.isInterface = topBlock.isInterface();
        this.isEnum = topBlock.isEnum();
        this.packageName = topBlock.getPackageName();
        this.className = topBlock.getClassName();
        if (!isInterface() && !isEnum()) {
            for (Block b : blockStructure[1]) {

                if (className == null) {
                    // System.out.println(path + " +++++ERRROR+++++(CLASSNAME=NULL)++++");
                    // System.out.println(src);
                } else {
                    b.init(className);
                    if (b.isMethod()) {
                        methodCount++;
                        maxLine = Math.max(maxLine, b.line);
                    }
                }
                // System.out.println(b.isMethod() + " " + b);
            }
        }
    }

    public String getPackageName() {
        return packageName;
    }

    public String getClassName() {
        return className;
    }

    public boolean isEnum() {
        return isEnum;
    }

    public boolean isInterface() {
        return isInterface;
    }

    public boolean isInnerClass() {
        return false;
    }

    public int getMinLine() {
        return minLine;
    }

    public int getMaxLine() {
        return maxLine;
    }

    public int getMethodCount() {
        return methodCount;
    }

    public boolean hasConstructor() {
        for (Block b : blockStructure[1]) {
            if (b.isConstructor()) {
                return true;
            }
        }
        return false;
    }

    public String toTightString() {
        return toString(true);
    }

    public String toLooseString() {
        return toString(false);
    }

    public String toString() {
        return toTightString();
    }

    public long getSize() {
        return size;
    }

    public String toString(boolean tight) {
        StringBuilder buf = new StringBuilder(1024);
        buf.append(topBlock.getClassLine(tight)).append('\n');
        int line = topBlock.line;
        if (!hasConstructor()) {
            buf.append(Pad.left(" ", ' ', 5)).append(' ');
            buf.append(topBlock.getVisibility());
            buf.append(' ').append(className).append("()\n");
        }
        for (Block b : blockStructure[1]) {
            if (b.isMethod()) {
                buf.append(b.getMethodLine()).append('\n');
            }
        }
        return buf.toString();
    }

    public int compareTo(Src s) {
        String s1 = packageName;
        String s2 = s.packageName;
        int c = s1.compareTo(s2);
        if (c == 0) {
            s1 = className;
            s2 = s.className;
            c = s1.compareTo(s2);
        }
        return c;
    }

    public static ArrayList<Block>[] getBlockStructure(String s) {
        Stack<int[]> stack = new Stack<int[]>();
        ArrayList<Block>[] blocksByDepth = new ArrayList[4];
        for (int i = 0; i < blocksByDepth.length; i++) {
            blocksByDepth[i] = new ArrayList<Block>(32);
        }

        char[] C = s.toCharArray();
        int line = 1;
        for (int y = 0; y < C.length; y++) {
            char c = C[y];
            if (c == '\n') {
                line++;
            } else if (c == '{') {
                stack.push(new int[]{line, y});
            } else if (c == '}') {
                int[] pop = stack.pop();
                int depth = stack.size();
                if (depth <= 3) {
                    int w = -1;
                    int x = pop[1];
                    int blockLine = pop[0];
                    int len = blocksByDepth[depth].size();

                    if (len > 0) {
                        w = blocksByDepth[depth].get(len - 1).y;
                    }
                    if (depth > 0) {
                        w = Math.max(w, stack.peek()[1]);
                    }

                    Block b = new Block(s, blockLine, depth, w, x, y);
                    blocksByDepth[depth].add(b);
                }
            }
        }

        if (!stack.isEmpty()) {
            throw new RuntimeException("stack not empty!");
        }

        return blocksByDepth;
    }

    private static String cleanCommentsAndLiterals(String s) {
        final char LF = '\n';
        final char CR = '\r';
        final char SINGLE_QUOTE = '\'';
        final char DOUBLE_QUOTE = '"';
        final char SLASH = '/';
        final char STAR = '*';
        final char ANNOTATION = '@';
        final char ANNOTATION_BRACKET = '(';
        int bracketLevel = 0;

        final StringBuilder buf = new StringBuilder(s.length());
        StringBuilder annotationBuf = new StringBuilder();
        char[] C = s.toCharArray();

        // 5 modes:
        //
        // 0 = normal
        // SINGLE_QUOTE
        // DOUBLE_QUOTE
        // ANNOTATION // @
        // SLASH // comments
        // STAR  /* comments
        int mode = 0;
        int slashCount = 0;
        for (int i = 0; i < C.length; i++) {
            char c = C[i];
            if (mode == 0) {
                switch (c) {
                    case SINGLE_QUOTE:
                    case DOUBLE_QUOTE:
                    case ANNOTATION:
                        mode = c;
                        break;
                    case SLASH:
                        // have to look forward:
                        if (i + 1 < C.length) {
                            char cc = C[i + 1];
                            if (cc == SLASH || cc == STAR) {
                                mode = cc;
                            }
                        }
                }
            } else {

                if (mode == ANNOTATION) {

                    char nextC = 0;
                    if (i + 1 < C.length) {
                        nextC = C[i + 1];
                    }

                    if (c == '(' || (Character.isWhitespace(c) && nextC == '(')) {
                        mode = ANNOTATION_BRACKET;
                        if (c == '(') {
                            bracketLevel++;
                        }
                        annotationBuf = new StringBuilder();
                    } else if (Character.isWhitespace(c)) {
                        mode = 0;
                        String annotation = annotationBuf.toString();
                        if ("@interface".equalsIgnoreCase(annotation)) {
                            String str = " interface";
                            int pos = buf.length() - str.length();
                            String r1 = buf.toString().substring(pos, buf.length());
                            buf.replace(pos, buf.length(), str);
                            String r2 = buf.toString().substring(pos, buf.length());
                        }
                        annotationBuf = new StringBuilder();
                    }
                } else {

                    switch (c) {
                        case '(':
                            if (mode == ANNOTATION_BRACKET) {
                                bracketLevel++;
                            }
                            break;
                        case ')':
                            if (mode == ANNOTATION_BRACKET) {
                                bracketLevel--;
                                if (bracketLevel <= 0) {
                                    mode = 0;
                                    c = ' ';
                                }
                            }
                            break;
                        case SINGLE_QUOTE:
                        case DOUBLE_QUOTE:
                            if (mode == c && slashCount % 2 != 1) {
                                mode = 0;
                            }
                            break;
                        case LF:
                        case CR:
                            if (mode == SLASH) {
                                mode = 0;
                            }
                            break;
                        case SLASH:
                            if (mode == STAR) {
                                // have to look backwards:
                                char cc = C[i - 1];
                                if (cc == STAR) {
                                    mode = 0;
                                    c = ' ';
                                }
                            }
                    }
                }
            }

            if (c == '\\') {
                slashCount++;
            } else {
                slashCount = 0;
            }

            if (mode == 0) {
                buf.append(c);
            } else {
                // Comments are blanked (preserving LF / CR).
                if (mode == SLASH || mode == STAR || mode == ANNOTATION || mode == ANNOTATION_BRACKET) {
                    if (c == LF || c == CR) {
                        buf.append(c);
                    } else {
                        buf.append(' ');
                        if (mode == ANNOTATION) {
                            annotationBuf.append(c);
                        }
                    }
                } else {
                    // String literals are rewritten.
                    switch (c) {
                        case '{':
                        case '}':
                        case '(':
                        case ')':
                        case ':':
                        case ',':
                        case ';':
                        case '[':
                        case ']':
                        case '+':
                        case '-':
                        case '/':
                        case '*':
                        case '%':
                        case '^':
                        case '~':
                        case '=':
                        case '!':
                        case '&':
                        case '|':
                            buf.append('#');
                            break;
                        default:
                            if (Character.isLetter(c)) {
                                buf.append('Z');
                            } else if (Character.isDigit(c)) {
                                buf.append('9');
                            } else {
                                buf.append(c);
                            }
                    }
                }
            }
        }
        return buf.toString();
    }

    public static void main(String[] args) throws Exception {
        File f = new File(args[0]);
        scan(f);
    }

    public static void scan(File d) throws Exception {
        File[] files = d.listFiles();
        if (files == null) {
            files = new File[]{d};
        }
        for (File f : files) {
            if (f.isDirectory()) {
                scan(f);
            } else {
                String name = f.getName();
                if (name.endsWith(".java") && name.indexOf('-') < 0) {
                    try {
                        Src s = new Src(f);
                        System.out.println(s.toTightString());
                    } catch (Exception e) {
                        System.out.println("***ERR: " + f.getCanonicalPath());
                    }

                }
            }
        }
    }

}
