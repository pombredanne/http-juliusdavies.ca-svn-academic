package ca.juliusdavies.csc589d.jarchive;

import ca.phonelist.util.Pad;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

/**
 * @author Julius Davies
 * @since Apr 16, 2010
 */
final public class Block {

    final String src;
    final int line;
    final int d;
    final int w;
    final int x;
    final int y;

    private String packageName = null;
    private String className = null;
    private String extendsInfo = "java.lang.Object";
    private ArrayList<String> implementsList = null;
    private Boolean isClass = null;

    private String methodName = null;
    private String methodReturn = null;
    private String methodParams = null;

    private boolean isInterface = false;
    private boolean isEnum = false;
    private boolean isPublic = false;
    private boolean isProtected = false;
    private boolean isPrivate = false;
    private boolean isSynchronized = false;
    private boolean isStatic = false;
    private boolean isNative = false;
    private boolean isFinal = false;
    private boolean isAbstract = false;

    private Boolean isMethod = null;
    private boolean isConstructor = false;
    private ArrayList<String> paramList = null;
    private ArrayList<String> throwsList = null;

    public Block(String src, int line, int d, int w, int x, int y) {
        this.src = src;
        this.line = line;
        this.d = d;
        this.w = w;
        this.x = x;
        this.y = y;
    }

    public void init(String className) {
        parsePackageAndClassInfo();
        parseMethodInfo(className);
    }

    void parseMethodInfo(final String extractedClass) {
        if (d != 1 || isMethod != null) { return; }

        // Clean the class name:
        StringBuilder buf = new StringBuilder();
        char[] C = extractedClass.trim().toCharArray();
        for (char c : C) {
            if (!Character.isJavaIdentifierPart(c)) {
                break;
            }
            buf.append(c);
        }
        this.className = buf.toString();
        final int classNameLen = className.length();

        String methodLine = src.substring(w + 1, x + 1).trim();
        int i = methodLine.lastIndexOf(';'), j = -1;
        if (i >= 0) {
            methodLine = methodLine.substring(i + 1).trim();
        }

        // methods don't have = sign at this point.
        i = methodLine.indexOf('=');
        if (i >= 0) {
            isMethod = Boolean.FALSE;
        }
        // methods don't have 'class' at this point
        if (isMethod == null) {
            i = methodLine.indexOf("class");

            boolean charBeforeIsSpace = true;
            boolean charAfterIsSpace = true;

            if (i > 0) {
                char c = methodLine.charAt(i - 1);
                charBeforeIsSpace = !Character.isJavaIdentifierPart(c);
            }

            if (i + 5 < methodLine.length()) {
                char c = methodLine.charAt(i + 5);
                charAfterIsSpace = !Character.isJavaIdentifierPart(c);
            }

            if (i >= 0 && charBeforeIsSpace && charAfterIsSpace) {
                isMethod = Boolean.FALSE;
            }
        }
        // no '=' and no 'class' therefore must be method!
        if (isMethod == null) {
            isMethod = Boolean.TRUE;
        }

        if (isMethod) {
            i = methodLine.indexOf('(');
            j = methodLine.lastIndexOf(')');
            if (i < 0) {
                // No parentheses:  it's a static initializer
                isMethod = Boolean.FALSE;
            }
        }

        if (isMethod) {
            String pre = methodLine.substring(0, i);
            String inner = methodLine.substring(i + 1, j);
            String post = methodLine.substring(j + 1);

            boolean modifiersFinished = false;

            // Parse the stuff before the '('
            String[] S = pre.split("\\s+");
            for (i = 0; i < S.length; i++) {
                String s = S[i];

                if (!modifiersFinished) {
                    modifiersFinished = parseModifiers(s);
                }

                if (modifiersFinished) {

                    // Put remaining string back together (don't want to split on spaces)
                    // except for last token.
                    StringBuilder backTogether = new StringBuilder(64);
                    for (; i < S.length - 1; i++) {
                        backTogether.append(S[i]).append(' ');
                    }
                    s = backTogether.toString();

                    methodName = S[S.length - 1];
                    isConstructor = isConstructor(className, classNameLen, methodName);

                    if (isConstructor) {
                        methodName = className;
                    } else {
                        methodReturn = Clean.returnType(s);
                    }
                }
            }

            // Parse the stuff inside the '(' and ')'
            inner = Clean.generics(inner);
            String[] PARAMS = inner.split(",");
            paramList = new ArrayList<String>();
            for (String param : PARAMS) {

                // varargs:
                param = param.replace("...", "[]");

                S = param.trim().split("\\s+");
                int pos = 0;
                if ("final".equals(S[0])) {
                    pos = 1;
                }

                boolean isArray = param.indexOf('[') >= 0;
                param = S[pos];
                int ix = param.indexOf('[');
                if (ix >= 0) {
                    param = param.substring(0, ix);
                }
                param = isArray ? param + "[]" : param;
                paramList.add(param);

            }

            // Parse the stuff after the ')'
            post = post.trim();
            if (post.charAt(post.length() - 1) == '{') {
                post = post.substring(0, post.length() - 1);
            }
            String[] THROWS = post.split(",");
            throwsList = new ArrayList<String>();
            for (String t : THROWS) {
                S = t.trim().split("\\s+");
                for (String s : S) {
                    s = s.trim();
                    if (!"throws".equals(s) && !"".equals(s)) {
                        throwsList.add(Clean.returnType(s));
                    }
                }
            }

        }

    }

    private boolean parseModifiers(String s) {
        if ("public".equals(s)) {
            isPublic = true;
        } else if ("protected".equals(s)) {
            isProtected = true;
        } else if ("private".equals(s)) {
            isPrivate = true;
        } else if ("synchronized".equals(s)) {
            isSynchronized = true;
        } else if ("static".equals(s)) {
            isStatic = true;
        } else if ("native".equals(s)) {
            isNative = true;
        } else if ("final".equals(s)) {
            isFinal = true;
        } else if ("abstract".equals(s)) {
            isAbstract = true;
        } else {
            // parsing of modifiers is finished
            return true;
        }
        // there still might be more modifiers
        return false;
    }

    private static boolean isConstructor(
        final String className, final int classNameLen, final String s
    ) {
        return
            s.startsWith(className) && (
                s.length() == classNameLen || !Character.isDigit(s.charAt(classNameLen))
            );
    }

    void parsePackageAndClassInfo() {
        if (d != 0 || className != null) { return; }

        String blockPrefix = src.substring(w + 1, x + 1);
        String[] STMTS = blockPrefix.split(";");
        for (String stmt : STMTS) {
            String[] S = stmt.split("\\s+");

            int px = -1;
            int ix = -1;
            int cx = -1;
            px = indexOf(S, "package");
            if (px < 0) {
                ix = indexOf(S, "import");
                if (ix < 0) {
                    cx = indexOf(S, "class");
                    if (cx < 0) {
                        cx = indexOf(S, "interface");
                        if (cx < 0) {
                            cx = indexOf(S, "@interface");
                        }
                        if (cx >= 0) {
                            isInterface = true;
                        } else {
                            cx = indexOf(S, "enum");
                            if (cx >= 0) {
                                isEnum = true;
                            }
                        }
                    }
                }
            }

            if (px >= 0) {
                packageName = S[px + 1];
            }
            if (cx >= 0) {
                for (int i = 0; i <= cx; i++) {
                    String s = S[i];
                    parseModifiers(s);
                }

                className = S[cx + 1];
                for (int i = cx + 2; i < S.length; i++) {
                    String s = S[i];
                    if ("extends".equals(s)) {
                        extendsInfo = S[i + 1];
                    } else if ("implements".equals(s)) {
                        implementsList = new ArrayList<String>();

                        // Put remaining string back together (don't want to split on spaces)
                        StringBuilder buf = new StringBuilder(64);
                        for (++i; i < S.length; i++) {
                            buf.append(S[i]).append(' ');
                        }

                        // Split on ',' instead.
                        S = Clean.generics(buf.toString()).split(",");
                        for (String tok : S) {
                            int brace = tok.indexOf('{');
                            if (brace >= 0) {
                                tok = tok.substring(0, brace);
                            }
                            tok = tok.trim();
                            implementsList.add(Clean.returnType(tok));
                        }

                    }
                }
            }
        }
        this.isClass = className != null ? Boolean.TRUE : Boolean.FALSE;
    }

    private int indexOf(String[] S, String k) {
        for (int i = 0; i < S.length; i++) {
            String s = S[i];
            if (k.equals(s)) {
                return i;
            }
        }
        return -1;
    }

    public String getVisibility() {
        if (isPublic) {
            return "public";
        } else if (isProtected) {
            return "protected";
        } else if (isPrivate) {
            return "private";
        } else {
            return "default";
        }
    }
    public String getMethodName() { return isConstructor ? className : methodName; }
    public String getMethodReturn() { return methodReturn; }
    public String getMethodParams() { return methodParams; }
    public boolean isInterface() { return isInterface; }
    public boolean isEnum() { return isEnum; }
    public boolean isMethod() { return isMethod != null && isMethod; }
    public boolean isConstructor() { return isConstructor; }
    public String getModifierLine() {
        StringBuilder buf = new StringBuilder();
        if (isPublic) {
            buf.append("public ");
        } else if (isProtected) {
            buf.append("protected ");
        } else if (isPrivate) {
            buf.append("private ");
        } else {
            buf.append("default ");
        }

        if (isAbstract) { buf.append("abstract "); }
        if (isFinal) { buf.append("final "); }
        if (isNative) { buf.append("native "); }
        if (isStatic) { buf.append("static "); }
        if (isSynchronized) { buf.append("synchronized "); }

        return buf.toString();
    }

    public String getPackageName() { return packageName; }
    public String getClassName() { return Clean.generics(className); }
    public String getExtendsInfo() { return extendsInfo; }

    public String getClassLine(boolean tight) {
        if (isClass == null || !isClass) {
            return null;
        }

        StringBuilder buf = new StringBuilder(128);
        buf.append(getModifierLine());

        if (tight) {
            String pkg = getPackageName();
            if (pkg != null && !"".equals(pkg)) {
                buf.append(pkg).append('.');
            }
        }
        buf.append(getClassName());
        buf.append(" extends ").append(Clean.returnType(getExtendsInfo()));
        if (implementsList != null && !implementsList.isEmpty()) {
            buf.append(" implements ");
            Collections.sort(implementsList);
            for (String s : implementsList) {
                buf.append(s).append(',');
            }
            buf.setCharAt(buf.length() - 1, ' ');
        }
        return buf.toString().trim();
    }

    private void parseAnnotation(boolean deprecated, String orig) {
        /*
@Overrideprotectedfinal
@Overrideprotectedsynchronized
@Overrideprotected

@Overridepublicsynchronized
@Overridepublicfinal
@Overridepublic
         */

        if (deprecated) {
            orig = orig.substring("@Deprecated".length()).trim();
        } else {
            orig = orig.substring("@Override".length()).trim();
        }
        String s = orig.toLowerCase(Locale.ENGLISH);
        if (s.startsWith("protected")) {
            isProtected = true;
            s = s.substring("protected".length());
        } else if (s.startsWith("public")) {
            isPublic = true;
            s = s.substring("public".length());
        }

        // 1st pass
        if (s.startsWith("synchronized")) {
            isSynchronized = true;
            s = s.substring("synchronized".length());
        }
        if (s.startsWith("final")) {
            isFinal = true;
            s = s.substring("final".length());
        }
        if (s.startsWith("static")) {
            isStatic = true;
            s = s.substring("static".length());
        }

        // 2nd pass
        if (s.startsWith("synchronized")) {
            isSynchronized = true;
            s = s.substring("synchronized".length());
        }
        if (s.startsWith("final")) {
            isFinal = true;
            s = s.substring("final".length());
        }
        if (s.startsWith("static")) {
            isStatic = true;
            s = s.substring("static".length());
        }

        methodReturn = orig.substring(orig.length() - s.length());

    }

    public java.lang.String getMethodLine() {
        if (isMethod == null || !isMethod) {
            return null;
        }

        StringBuilder buf = new StringBuilder(128);
        buf.append(Pad.left(" ", ' ', 5)).append(' ');
        buf.append(getModifierLine());
        if (!isConstructor) {
            String returnType = getMethodReturn();
            buf.append(Clean.returnType(returnType)).append(' ');
        }
        buf.append(getMethodName());
        buf.append('(');
        for (String s : paramList) {
            buf.append(Clean.returnType(s)).append(',');
        }
        buf.setCharAt(buf.length() - 1, ')');

        if (throwsList != null && !throwsList.isEmpty()) {
            buf.append(" throws ");

            Collections.sort(throwsList);
            for (String s : throwsList) {
                buf.append(s).append(',');
            }
            buf.setCharAt(buf.length() - 1, ' ');
        }
        return buf.toString();

    }

    public String toString() {
        String prefix = src.substring(w + 1, x + 1);
        return "\n" + d + " line=" + line + " (" + x + "," + y + ") \n" + prefix + "\n";
    }

}
