package ca.juliusdavies.csc589d.jarchive;
import ca.phonelist.util.Util;
import org.apache.commons.csv.CSVParser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class HotQuery {

    private final static String HEADER = "SET statement_timeout = 0;\n" +
        "SET client_encoding = 'UTF8';\n" +
        "SET standard_conforming_strings = off;\n" +
        "SET check_function_bodies = false;\n" +
        "SET client_min_messages = warning;\n" +
        "SET escape_string_warning = off;\n" +
        "\n" +
        "SET search_path = public, pg_catalog;\n" +
        "\n" +
        "SET default_tablespace = '';\n" +
        "\n" +
        "SET default_with_oids = false;\n" +
        "\n" +
        "CREATE TABLE max (\n" +
        "    jar character varying(255) NOT NULL,\n" +
        "    pkg character varying(255) NOT NULL,\n" +
        "    name character varying(255) NOT NULL,\n" +
        "    tightsig character(27) NOT NULL,\n" +
        "    srcLic character varying(255) NOT NULL,\n" +
        "    googLic  character varying(255) NOT NULL,\n" +
        "    collision character varying(255) NOT NULL\n" +
        ");\n" +
        "\n" +
        "\n" +
        "ALTER TABLE public.max OWNER TO jarchive2;\n" +
        "\n" +
        "\n" +
        "COPY max (jar, pkg, name, tightsig, srcLic, googLic, collision) FROM stdin;";

    private static Map<String, Map<String, String>> parseNinka(String fileName) throws Exception {
        FileInputStream fin = null;
        InputStreamReader isr = null;
        BufferedReader br = null;

        Map<String, Map<String, String>> choices = new TreeMap<String, Map<String, String>>();
        try {
            fin = new FileInputStream(fileName);
            isr = new InputStreamReader(fin, "UTF-8");
            br = new BufferedReader(isr);

            String line = br.readLine();
            while (line != null) {

                line = line.trim();
                if (line.startsWith("./")) {
                    line = line.substring(2);
                }

                int x = line.indexOf('/');
                if (x >= 0) {

                    String zip = line.substring(0, x);
                    int z = line.indexOf(".java;");
                    if (z >= 0) {

                        String path = line.substring(x, z);
                        int y = path.lastIndexOf('/');
                        if (y >= 0) {

                            String name = line.substring(x + y + 1, z);
                            x = line.indexOf(';', z + 6);
                            if (x < 0) {
                                x = line.length();
                            }

                            String lic = line.substring(z + 6, x);

                            Map<String, String> m = choices.get(zip);
                            if (m == null) {
                                m = new TreeMap<String, String>();
                                choices.put(zip, m);
                            }

                            m.put(name, lic);

                        }
                    }
                }

                line = br.readLine();
            }
        } finally {
            Util.close(br, isr, fin);
        }

        return choices;
    }

    private static Map<String, String> parseChoices(String fileName) throws Exception {
        FileInputStream fin = null;
        InputStreamReader isr = null;
        BufferedReader br = null;

        Map<String, String> choices = new TreeMap<String, String>();
        try {
            fin = new FileInputStream(fileName);
            isr = new InputStreamReader(fin, "UTF-8");
            br = new BufferedReader(isr);

            String line = br.readLine();
            while (line != null) {

                line = line.trim();
                if (line.indexOf('=') >= 0) {
                    int x = line.indexOf('=');
                    String jar = line.substring(0, x).trim();
                    String src = line.substring(x + 1).trim();
                    choices.put(jar, src);
                }

                line = br.readLine();
            }
        } finally {
            Util.close(br, isr, fin);
        }

        return choices;
    }

    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();

        Set<String> missing = new TreeSet<String>();

        Map<String, Map<String, String>> ninkaJar2name2lic = parseNinka("oracle-ninka-raw.txt");
        Map<String, String> choicesMatch = parseChoices("choices-match.txt");
        Map<String, String> choicesSrc = parseChoices("choices-src.txt");
        Map<String, Map<String, String>> googleJar2name2lic = new TreeMap<String, Map<String, String>>();

        FileInputStream fin = new FileInputStream(args[0]);
        InputStreamReader isr = new InputStreamReader(fin, "UTF-8");
        BufferedReader br = new BufferedReader(isr);

        CSVParser csv = new CSVParser(br);
        String[] toks = csv.getLine();
        while (toks != null) {
            String jar = toks[1];
            String name = toks[2];
            String googleLic = toks[4];
            Map<String, String> m = googleJar2name2lic.get(jar);
            if (m == null) {
                m = new TreeMap<String, String>();
                googleJar2name2lic.put(jar, m);
            }
            m.put(name, googleLic);
            toks = csv.getLine();
        }
        br.close();
        isr.close();
        fin.close();

        long delay = System.currentTimeMillis() - start;
        // System.out.println("Done.  Took " + delay + "ms");

        HashMap<String, Integer> jarNameCollisions = new HashMap<String, Integer>();

        fin = new FileInputStream(args[1]);
        isr = new InputStreamReader(fin, "UTF-8");
        br = new BufferedReader(isr);
        String readLine = br.readLine();

        ArrayList<String> lines = new ArrayList<String>(32 * 1024);
        while (readLine != null) {
            lines.add(readLine);
            readLine = br.readLine();
        }

        for (String line : lines) {
            toks = line.split(";");
            String jar = toks[0];
            String name = toks[2];
            int x = name.indexOf('$');
            if (x < 0) {
                String collisionKey = jar + "#" + name;
                Integer i = jarNameCollisions.get(collisionKey);
                if (i == null) {
                    jarNameCollisions.put(collisionKey, 1);
                } else {
                    jarNameCollisions.put(collisionKey, i + 1);
                }
            }
        }

        System.out.println(HEADER);

        for (String line : lines) {
            boolean collision = false;
            toks = line.split(";");
            String jar = toks[0];

            String pkg = toks[1];
            String name = toks[2];
            String sig = toks[3];
            if ("NULL".equals(sig)) {
                sig = "0";
            }
            String k = name;
            int x = k.indexOf('$');
            if (x >= 0) {
                k = k.substring(0, x);
            } else {
                String collisionKey = jar + "#" + k;
                Integer i = jarNameCollisions.get(collisionKey);
                collision = (i != null && i > 1);
            }

            String realLic = null;
            String src = choicesSrc.get(jar);

            Map<String, String> m = ninkaJar2name2lic.get(src);
            if (m == null) {
                realLic = "NO-SRC";
            }
            if (m != null) {
                realLic = m.get(k);
            }
            if (realLic == null) {
                realLic = "NO-LIC";
            }

            String googleLic = null;

            // BIG TODO:  FIX THIS!!!!!
            if (jar.equals("commons-cli-1.0.jar")) {
                jar = "commons-cli-1.2.jar";
            } else if (jar.equals("javax.xml.ws_2.0.0.v200806030422.jar")) {
                jar = "javax.xml.ws_2.0.0.v200902170419.jar";
            }

            m = googleJar2name2lic.get(jar);
            if (m == null) {
                missing.add(jar);
            }
            if (m != null) {
                googleLic = m.get(k);
            }
            if (googleLic == null) {
                googleLic = "NO-GOOG";
            }

            StringBuilder buf = new StringBuilder(line.length() * 2);
            buf.append(jar).append("\t");
            buf.append(pkg).append("\t");
            buf.append(name).append("\t");
            buf.append(sig).append("\t");

            realLic = realLic.replace('\t', ' ');
            if (realLic.indexOf("Apache Liusercense, Version 2.0") > 0) {
                realLic = "Apachev2";
            } else if (realLic.indexOf("Artistic License") > 0) {
                realLic = "Artistic";
            } else if (realLic.indexOf("Apache JServ servlet") > 0) {
                realLic = "Apachev1.0";
            } else if (realLic.equals("UUNMATCHED [UNKNOWN,0,UNKNOWN,1,No warranty")) {
                realLic = "publicDomain"; // No warranty; no copyright -- use this as you will.
            } else if (realLic.endsWith("SeeFile,SeeFile")) {
                realLic = realLic.substring(0, realLic.length() - 8);
            } else if (realLic.indexOf("XXXLGPLv2.1+") > 0) {
                realLic = "LesserGPLv2.1+";
            } else if (realLic.indexOf("class SegmentTermVector implements") > 0) {
                realLic = "NONE";
            } else if (realLic.indexOf("int numTerms = tvf.readVInt()") > 0) {
                realLic = "NONE";
            } else if (realLic.indexOf("<CODE> for each document") > 0) {
                realLic = "NONE";
            } else if (realLic.indexOf("[UNKNOWN,0,UNKNOWN,1,nil") > 0) {
                realLic = "Apachev1.x Variant (Indiana-University-1.1.1)";
            } else if (realLic.indexOf("See the bottom of this file") > 0) {
                realLic = "Apachev1.x Variant (Indiana-University-1.2)";
            } else if (realLic.indexOf("Eclipse Public License v1.0") > 0) {
                realLic = "EPLv1";
            } else if (realLic.indexOf("www.w3.org/Consortium/Legal") > 0) {
                realLic = "W3C";
            } else if (realLic.indexOf("ApacheLic1_1") > 0) {
                realLic = "Apachev1.1";
            } else if (realLic.indexOf("@see WireFeed for details") > 0) {
                realLic = "NONE";
            } else if (realLic.indexOf("Public Domain") > 0) {
                realLic = "publicDomain";
            } else if (realLic.indexOf("Systemics + Added Geoffrey Keating") > 0) {
                realLic = "Madness";
            } else if (realLic.indexOf("Visigoth Software Society") > 0) {
                realLic = "Apachev1.x Variants (Freemarker)";
            } else if (realLic.indexOf("JDOM") > 0) {
                realLic = "Apachev1.1-JDOM (w/o-Advertising-Clause)";
            } else if (realLic.indexOf("If the object is to be distributed or persisted") > 0) {
                realLic = "NONE";
            } else if (realLic.indexOf("Apache style license") > 0) {
                realLic = "Apachev1.1";
            } else if (realLic.indexOf("See the reference datatype library implementation") > 0) {
                realLic = "NONE";
            } else if (realLic.indexOf("OpenSymphony Software License version 1.1") > 0) {
                realLic = "Apachev1.1";
            } else if (realLic.indexOf("Its parent class loader") > 0) {
                realLic = "NONE";
            } else if (jar.equals("lucene-1.4.1.jar") && realLic.contains("UUNMATCHED [UNKNOWN,0,UNKNOWN,1")) {
                realLic = "NONE";
            } else if (realLic.indexOf("UUNMATCHED") >= 0) {
                realLic = "UUNMATCHED";
            }

            buf.append(realLic).append("\t");
            buf.append(googleLic).append("\t");
            buf.append(collision ? "COLLISION" : "");
            System.out.println(buf);
        }

        System.out.println("\\.\n\n");

        for (String s : missing) {
            System.out.println(s);
        }
    }

}
