package ca.juliusdavies.csc589d.jarchive;

import java.util.LinkedList;

/**
 * @author Julius Davies
 * @since Apr 16, 2010
 */
final class Stack<E> {

    private final LinkedList<E> list = new LinkedList<E>();

    Stack () {}

    public void push() { list.addFirst(null); }
    public void push(E o) { list.addFirst(o); }
    public E peek() { return list.getFirst(); }    
    public E pop() { return list.removeFirst(); }
    public int size() { return list.size(); }
    public boolean isEmpty() { return list.isEmpty(); }

}
