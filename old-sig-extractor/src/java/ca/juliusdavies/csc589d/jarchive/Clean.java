package ca.juliusdavies.csc589d.jarchive;

/**
 * @author Julius Davies
 * @since Apr 17, 2010
 */
public class Clean {

    public static String type(String s) {
        if (s == null) {
            return null;
        }
        s = s.trim();

        // Probably a generic:
        if (s.length() == 1 && Character.isUpperCase(s.charAt(0))) {
            return "Object";
        }
        
        int x = s.lastIndexOf('.');
        if (x >= 0) {
            s = s.substring(x + 1);
        }

        x = s.indexOf("[]");
        if (x >= 0) {
            s = s.substring(0, x + 2);
        }        
        return s;
    }

    public static String generics(String s) {
        if (s == null) {
            return null;
        }
        s = s.trim();

        String backup = null;
        if (s.length() > 0) {
            if (s.charAt(0) == '<') {
                int x = s.indexOf("extends");
                if (x >= 0) {
                    int y = s.indexOf('>', x);
                    if (y >= 0) {
                        backup = s.substring(x + 7, y);
                    }
                }
            }
        }

        StringBuilder buf = new StringBuilder(s.length());
        Stack stack = new Stack();
        char[] C = s.toCharArray();
        for (char c : C) {
            switch (c) {
                case '<':
                    stack.push();
                    break;
                case '>':
                    if (!stack.isEmpty()) {
                        stack.pop();
                    }                    
                    break;
                default:
                    if (stack.size() == 0) {
                        buf.append(c);
                    }
            }
        }

        String val = buf.toString().trim();
        if (backup != null) {
            if (val.length() == 1 || (val.endsWith("[]") && val.length() == 3 )) {
                if (Character.isUpperCase(val.charAt(0))) {
                    backup = generics(backup);
                    if (backup != null && backup.length() > 1) {
                        return backup;
                    }
                }
            }
        }
        return val;

    }

    public static String returnType(String s) {
        if (s == null) {
            return null;
        }
        s = generics(s);
        int x = s.indexOf('$');
        if (x >= 0) {
            s = s.substring(x+1);
        }
        String[] S = s.split("\\s+");
        StringBuilder buf = new StringBuilder();
        for (String tok : S) {
            buf.append(tok);
        }        
        return Clean.type(buf.toString());
    }

}
