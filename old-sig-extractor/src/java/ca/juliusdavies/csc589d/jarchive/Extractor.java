package ca.juliusdavies.csc589d.jarchive;

import ca.phonelist.util.Bytes;
import ca.phonelist.util.Util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author Julius Davies
 * @since Apr 30, 2010
 */
public class Extractor {

    private static String prefix;
    private static String target;

    public static void main(String[] args) throws Exception {
        File f1 = new File(args[0]);
        File f2 = new File(args[1]);

        prefix = f1.getCanonicalPath();
        target = f2.getCanonicalPath();
        if (!f2.exists()) {
            f2.mkdirs();
        }
        scan(f1);
    }

    public static void scan(File d) throws IOException {
        if (d.isDirectory()) {
            File[] files = d.listFiles();
            if (files != null) {
                for (File f : files) {
                    scan(f);
                }
            }
        } else {

            final String path = d.getCanonicalPath();
            final String n = d.getName().toLowerCase(Locale.ENGLISH);

            boolean isZip = n.endsWith(".jar") || n.endsWith(".zip");
            boolean isLegal = isLegal(n);

            FileInputStream fin = null;
            ZipInputStream zin = null;
            try {
                if (isZip) {
                    fin = new FileInputStream(d);
                    zin = new ZipInputStream(fin);
                    scanZip(path, zin);
                } else if (isLegal) {
                    fin = new FileInputStream(d);
                    byte[] bytes = Bytes.streamToBytes(fin, false);
                    copy(path, bytes);
                }
            } finally {
                Util.close(zin, fin);
            }

        }
    }

    private static void scanZipEntry(
        String path, final ZipInputStream zin, final ZipEntry ze
    ) throws IOException {
        if (!ze.isDirectory()) {

            String n = ze.getName().trim().toLowerCase(Locale.ENGLISH);
            int x = n.lastIndexOf('/');
            if (x >= 0) {
                n = n.substring(x + 1);
            }

            boolean isLegal = isLegal(n);
            boolean isZip = n.endsWith(".jar") || n.endsWith(".zip");

            if (isZip) {

                byte[] zip = Bytes.streamToBytes(zin, false);
                InputStream in = new ByteArrayInputStream(zip);
                ZipInputStream newZ = null;
                try {
                    newZ = new ZipInputStream(in);
                    scanZip(path + "/" + ze.getName(), newZ);
                } finally {
                    Util.close(newZ);
                }

            }

            if (isLegal) {
                byte[] bytes = Bytes.streamToBytes(zin, false);
                copy(path + "/" + ze.getName(), bytes);
            }
        }
    }

    private static void scanZip(String path, final ZipInputStream zin) {
        try {
            ZipEntry ze = zin.getNextEntry();
            while (ze != null) {
                scanZipEntry(path, zin, ze);
                ze = zin.getNextEntry();
            }
        } catch (Throwable t) {
            System.out.println("zip err: " + path + " " + t);
            t.printStackTrace(System.out);
            if (t instanceof Error) {
                throw (Error) t;
            }
        }
    }

    private static void copy(String path, byte[] bytes) throws IOException {
        if (path.startsWith(prefix)) {
            path = path.substring(prefix.length());
            path = target + path;

            File f = new File(path);
            File p = f.getParentFile();
            if (!p.exists()) {
                p.mkdirs();
            }

            FileOutputStream fout = null;
            try {
                fout = new FileOutputStream(f);
                fout.write(bytes);
            } finally {
                Util.close(fout);
            }
            // System.out.println("Copying to: [" + path + "]");
        }
    }

    private static boolean isLegal(final String fileName) {
        String s = fileName.trim().toLowerCase(Locale.ENGLISH);
        boolean isLegal = false;
        if (s.endsWith(".txt")) {
            isLegal = true;
        }
        if (!isLegal) {
            int x = s.indexOf("copy");
            if (x < 0) {
                x = s.indexOf("notice");
                if (x < 0) {
                    x = s.indexOf("legal");
                    if (x < 0) {
                        x = s.indexOf("license");
                        if (x < 0) {
                            x = s.indexOf("licence");
                            if (x < 0) {
                                x = s.indexOf("readme");
                            }
                        }
                    }
                }
            }
            isLegal = x >= 0;
        }
        return isLegal;
    }
}
