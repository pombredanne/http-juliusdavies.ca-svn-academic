package ca.juliusdavies.csc589d.jarchive;

import ca.phonelist.util.Pad;
import ca.phonelist.util.Util;
import ca.phonelist.util.Bytes;
import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.ExceptionTable;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LineNumber;
import org.apache.bcel.classfile.LineNumberTable;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.Type;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

/**
 * @author Julius Davies
 * @since Mar 1, 2010
 */
class Bin extends Artifact<Bin> {

    private JavaClass jc = null;
    private int minLine = -1;
    private int maxLine = -1;
    private String looseString = null;
    private String tightString = null;
    private int size;

    public Bin(File f) throws IOException {
        this(
            f.getName(),
            f.getCanonicalPath(),
            f.lastModified(),
            new FileInputStream(f)
        );
    }

    public Bin(
        String name, String path, long date, InputStream in
    ) throws IOException {
        super(name, path, date);
        byte[] bytes = Bytes.streamToBytes(in);
        this.srcBytes = bytes;
        this.size = bytes.length;
        in = new ByteArrayInputStream(bytes);
        try {
            ClassParser p = new ClassParser(in, name);
            this.jc = p.parse();
            for (Method m : jc.getMethods()) {
                LineNumberTable lnt = m.getLineNumberTable();
                if (lnt != null) {
                    LineNumber[] LN = lnt.getLineNumberTable();
                    if (LN != null) {
                        if (LN.length > 0) {
                            int line = LN[0].getLineNumber();
                            if (minLine < 0) {
                                minLine = line;
                            } else {
                                minLine = Math.min(minLine, line);
                            }
                            maxLine = Math.max(maxLine, line);
                        }

                    }
                }
            }
        } catch (Exception e) {
            System.out.println("failed to create Bin: " + path + " " + name + " " + e);
            e.printStackTrace(System.out);
        } finally {
            try {
                this.tightString = privateToString(true);
                this.looseString = privateToString(false);                
            } finally {
                Util.close(in);
            }
        }
    }

    public String getPackageName() { return jc.getPackageName(); }

    public String getClassName() { return Clean.type(jc.getClassName()); }

    public boolean isInnerClass() { return jc == null || jc.getClassName().indexOf('$') >= 0; }

    public boolean isInterface() { return jc == null || jc.isInterface(); }

    public boolean isEnum() { return jc == null || jc.isEnum(); }

    public int getMinLine() { return minLine; }

    public int getMaxLine() { return maxLine; }

    public int getMethodCount() { return jc.getMethods().length; }

    public long getSize() { return size; }

    static {
        int i = 123;
    }

    public final static native void b();

    public String getClassLine(boolean tight) {
        StringBuilder buf = new StringBuilder(128);
        if (jc.isPublic()) {
            buf.append("public ");
        } else if (jc.isProtected()) {
            buf.append("protected ");
        } else if (jc.isPrivate()) {
            buf.append("private ");
        } else {
            buf.append("default ");
        }

        if (jc.isAbstract()) {
            buf.append("abstract ");
        }
        if (jc.isFinal()) {
            buf.append("final ");
        }
        if (jc.isStatic()) {
            buf.append("static ");
        }

        if (tight) {
            buf.append(jc.getClassName());
        } else {
            buf.append(Clean.type(jc.getClassName()));
        }
        buf.append(" extends ").append(Clean.returnType(jc.getSuperclassName()));
        String[] interfaceNames = jc.getInterfaceNames();
        if (interfaceNames != null && interfaceNames.length > 0) {
            buf.append(" implements ");

            for (int i = 0; i < interfaceNames.length; i++) {
                interfaceNames[i] = Clean.returnType(interfaceNames[i]);
            }
            Arrays.sort(interfaceNames);
            for (String s : interfaceNames) {
                buf.append(s).append(',');
            }
            buf.setCharAt(buf.length() - 1, ' ');
        }
        return buf.toString().trim();
    }

    public static boolean isIgnored(Method m) {
        String n = m.getName();
        return m.isAbstract() || m.isNative() || "<clinit>".equals(n) || n.indexOf('$') >= 0;
    }

    private synchronized static String getMethodLine(String className, Method m) {

        String name = m.getName();

        LineNumberTable lnt = m.getLineNumberTable();
        int line = -4;
        if (lnt != null) {
            line = -3;
            LineNumber[] LN = lnt.getLineNumberTable();
            if (LN != null) {
                line = -2;
                if (LN.length > 0) {
                    line = LN[0].getLineNumber();
                }
            }
        }
        /*
        if (line < 0) {
            return null;
        }
        */

        String visibility = "default";
        if (m.isPublic()) {
            visibility = "public";
        } else if (m.isProtected()) {
            visibility = "protected";
        } else if (m.isPrivate()) {
            visibility = "private";
        }

        String type = "";
        if (m.isAbstract()) { type += " abstract"; }
        if (m.isFinal()) { type += " final"; }
        if (m.isNative()) { type += " native"; }
        if (m.isStatic()) { type += " static"; }
        if (m.isSynchronized()) { type += " synchronized"; }

        StringBuilder buf = new StringBuilder(128);
        buf.append(Pad.left(" ", ' ', 5)).append(' ');
        buf.append(visibility);
        buf.append(type);

        if ("<init>".equals(name)) {
            name = Clean.type(className);
        } else {
            String returnType = m.getReturnType().toString();
            returnType = Clean.returnType(returnType);
            buf.append(' ').append(returnType);
        }

        buf.append(' ').append(name);
        buf.append('(');
        Type[] args = m.getArgumentTypes();
        if (args != null && args.length > 0) {
            for (Type t : m.getArgumentTypes()) {
                String arg = Clean.returnType(t.toString());
                buf.append(arg).append(',');
            }
            buf.setCharAt(buf.length() - 1, ')');
        } else {
            buf.append(')');
        }

        ExceptionTable et = m.getExceptionTable();
        ArrayList<String> exceptions = new ArrayList<String>();
        if (et != null && et.getExceptionNames().length > 0) {
            buf.append(" throws ");
            for (String s : et.getExceptionNames()) {
                exceptions.add(Clean.returnType(s));
            }

            Collections.sort(exceptions);
            for (String s : exceptions) {
                buf.append(s).append(',');
            }
            buf.setCharAt(buf.length() - 1, ' ');
        }
        return buf.toString();

    }

    private void s() {}

    public int compareTo(Bin b) {
        String s1 = jc.getClassName();
        String s2 = b.jc.getClassName();
        return s1.compareTo(s2);
    }

    private int getDog(String i) {return -1;}

    public static void getDog(Object i, String... args) {}

    public String getMethodKey(Method m) {
        Type[] args = consolidateTypes(m);
        StringBuilder buf = new StringBuilder();
        buf.append(m.getName());

        buf.append('(');
        buf.append(args.length);
        buf.append(')');
        return buf.toString();
    }

    private static Type[] consolidateTypes(Method m) {
        Type rt = m.getReturnType();
        Type[] args = m.getArgumentTypes();
        int typeCount = args.length;
        if (!Type.VOID.equals(rt)) {
            typeCount++;
        }
        Type[] consolidatedTypes = new Type[typeCount];
        int pos = 0;
        if (!Type.VOID.equals(rt)) {
            consolidatedTypes[pos++] = rt;
        }
        if (args.length > 0) {
            System.arraycopy(args, 0, consolidatedTypes, pos, args.length);
        }
        return consolidatedTypes;
    }

    public String toString() { return toTightString(); }
    public String toTightString() { return tightString; }
    public String toLooseString() { return looseString; }    

    private String privateToString(boolean tight) {
        if (jc == null) { return ""; }

        String className = jc.getClassName();
        StringBuilder buf = new StringBuilder(4096);
        buf.append(getClassLine(tight));
        buf.append('\n');

        Map<String, ArrayList<Method>> byArgCount = new HashMap<String, ArrayList<Method>>();
        ArrayList<Method> methods = new ArrayList<Method>();
        for (Method m : jc.getMethods()) {
            if (isIgnored(m)) {
                continue;
            }
            methods.add(m);
            String k = getMethodKey(m);

            ArrayList<Method> list = byArgCount.get(k);
            if (list == null) {
                list = new ArrayList<Method>();
                byArgCount.put(k, list);
            }
            list.add(m);
        }

        // Walk backwards through list of methods and remove those which
        // look like compiler added them for generics.  The moment we
        // find a method to keep we STOP.
        ListIterator<Method> it = methods.listIterator(methods.size());
        while (it.hasPrevious()) {
            Method m = it.previous();

            Type[] types = consolidateTypes(m);
            boolean hasObject = false;
            for (Type t : types) {
                if (Type.OBJECT.equals(t)) {
                    hasObject = true;
                    break;
                }
            }
            if (hasObject) {

                ArrayList<Method> list = byArgCount.get(getMethodKey(m));
                if (list != null && list.size() > 1) {
                    boolean shouldDelete = shouldDelete(m, list);
                    if (shouldDelete) {
                        it.remove();
                    } else {
                        break;
                    }

                }

            } else {
                break;
            }
        }

        ArrayList<String> methodLines = new ArrayList<String>();
        for (Method m : methods) {
            String line = getMethodLine(className, m);
            if (line != null) {
                methodLines.add(line);
            }
        }

        // Collections.sort(methodLines);
        for (String s : methodLines) {
            if (s != null) {
                buf.append(s).append('\n');
            }
        }
        return buf.toString();
    }

    public boolean shouldDelete(Method m, final ArrayList<Method> list) {
        Type[] types1 = consolidateTypes(m);

        for (Method meth : list) {
            if (!m.equals(meth)) {
                boolean shouldDelete = true;
                Type[] types2 = consolidateTypes(meth);
                for (int i = 0; i < types1.length; i++) {
                    Type t1 = types1[i];
                    Type t2 = types2[i];

                    if (!Type.OBJECT.equals(t1)) {
                        if (!t1.equals(t2)) {
                            shouldDelete = false;
                        }
                    }
                }
                if (shouldDelete) { return true; }
            }
        }
        return false;
    }

    public String getFQN() {
        return jc.getClassName();
    }

    public byte[] getSrcBytes() { return null; }

    public static void main(String[] args) throws Exception {
        File f = new File(args[0]);
        scan(f);
    }

    public static void scan(File d) throws Exception {
        File[] files = d.listFiles();
        if (files == null) {
            files = new File[]{d};
        }
        for (File f : files) {
            if (f.isDirectory()) {
                scan(f);
            } else {
                String name = f.getName();
                if (name.endsWith(".class") && name.indexOf('-') < 0) {
                    try {
                        Bin b = new Bin(f);
                        System.out.println(b.toTightString());
                    } catch (Exception e) {
                        System.out.println("***ERR: " + f.getCanonicalPath());
                    }

                }
            }
        }
    }

}
