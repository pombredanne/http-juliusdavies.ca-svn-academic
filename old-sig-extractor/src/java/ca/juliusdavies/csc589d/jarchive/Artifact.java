package ca.juliusdavies.csc589d.jarchive;

import java.util.Date;
import java.util.Locale;

/**
 * @author Julius Davies
 * @since Apr 18, 2010
 */
public abstract class Artifact<E> implements Comparable<E> {

    private long date;
    private String version;
    private String fileName;
    private String path;
    private String folder;
    protected byte[] srcBytes;

    Artifact() {}

    public Artifact(String fileName, String path, long date) {
        this.date = date;
        this.fileName = fileName;
        this.path = path;
        this.folder = "";

        // Walk path in reverse looking for zips/jars.
        String[] S = path.split("/");
        for (int i = S.length - 1; i >= 0; i--) {
            String s = S[i];
            String t = s.trim().toLowerCase(Locale.ENGLISH);
            if (t.endsWith(".zip") || t.endsWith(".jar") || t.endsWith(".gz") || t.endsWith(".bz2") || t.contains(".tar.") || t.endsWith(".tar")) {
                this.folder = s;
                break;
            }
        }
        this.version = extractVersion(folder, path);        
    }

    public abstract String getPackageName();
    public abstract String getClassName();
    public abstract String toTightString();
    public abstract String toLooseString();    
    public abstract int getMinLine();
    public abstract int getMaxLine();
    public abstract int getMethodCount();
    public abstract boolean isEnum();
    public abstract boolean isInterface();
    public abstract boolean isInnerClass();

    public byte[] getSrcBytes() { return srcBytes; }
    public String getFQN() { return getPackageName() + "." + getClassName(); }
    public String getFileName() { return fileName; }
    public String getPath() { return path; }
    public String getFolder() { return folder; }
    public String getVersion() { return version; }
    public Date getDate() { return new Date(date); }
    public abstract long getSize();



    public static String extractVersion(String zipName, String path) {
        String version = "";
        String[] S = path.split("/");
        for (int i = S.length - 1; i >= 0; i--) {
            String s = S[i].trim();
            if (s.length() > 0) {
                char c = s.charAt(0);
                if (Character.isDigit(c)) {
                    version = s;
                    break;
                }
            }
        }
        if ("".equals(version) && zipName != null) {
            S = zipName.split("-");
            for (int i = 0; i < S.length; i++) {
                String s = S[i].trim();
                if (s.length() > 0) {
                    char c = s.charAt(0);
                    if (Character.isDigit(c)) {
                        i++;
                        for (; i < S.length; i++) {
                            s += "-" + S[i];
                        }
                        int x = s.lastIndexOf('.');
                        if (x >= 0) {
                            s = s.substring(0, x);
                        }
                        version = "*" + s;
                        break;
                    }
                }
            }

            if ("".equals(version)) {
                int x = zipName.lastIndexOf('-');
                if (x >= 0) {
                    x++;
                    int y = zipName.lastIndexOf('.');
                    if (y < 0) {
                        y = zipName.length();
                    }
                    if (x < zipName.length()) {
                        version = "**" + zipName.substring(x, y);
                    }
                }
            }
        }
        return version;
    }

}
