package ca.juliusdavies.signature;

import ca.phonelist.util.NullSafe;

import java.util.Comparator;

public class SignatureLine implements Comparable<SignatureLine> {
    public static final Comparator<SignatureLine> BY_LINE = new Comparator<SignatureLine>() {
        public int compare(SignatureLine s1, SignatureLine s2) {
            return NullSafe.compare(s1.line, s2.line);
        }
    };

    public static final Comparator<SignatureLine> BY_NAME = new Comparator<SignatureLine>() {
        public int compare(SignatureLine s1, SignatureLine s2) {
            return NullSafe.compare(s1.name, s2.name);
        }
    };

    public final String name;
    public final String line;

    public SignatureLine(String name, String line) {
        this.name = name;
        this.line = line;
    }

    public int compareTo(SignatureLine other) { return BY_NAME.compare(this, other); }

}
