package ca.juliusdavies.signature;

import ca.phonelist.util.Numbers;
import ca.phonelist.util.Util;

import java.io.*;
import java.util.*;

public class SimpleScan {

    // Are we using command-line args, or a list of filename on stdin?
    // ArgScanner deals with this distinction.
    public static class ArgScanner {
        private BufferedReader br;
        private int pos = 0;
        private String[] args;

        public ArgScanner(BufferedReader br) { this.br = br; }
        public ArgScanner(String[] args) { this.args = args; }

        public String nextArg() throws IOException {
            if (br == null) {
                return pos < args.length ? args[pos++] : null;
            } else {
                return br.readLine();
            }
        }
    }

    /*

c ; level ; name ; signature
m ; etc............

     */
    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.err.println();
            System.err.println("The Java Signature Extractor!");
            System.err.println("Extracts signatures from *.class and *.java.");
            System.err.println("by Julius Davies and Daniel M. German, July 2011.");
            System.err.println();
            System.err.println("Usage: [flags] [paths-to-examine...] ");
            System.err.println();
            System.err.println("Flags can be any combination of the following:");
            System.err.println("  --stdin                    Reads paths from stdin.");
            System.err.println("  --sortOutput       / -so   Sorts output (all signatures) by FQN.");
            System.err.println("  --sortInnerClasses / -si   Sorts inner-classes by name within each signature.");
            System.err.println("  --sortMethods      / -sm   Sorts methods by name within each signature.");
            System.err.println("  --sortFields       / -sf   Sorts fields by name within each signature.");
            System.err.println();
            System.err.println("  If data is supplied on STDIN, the extractor assumes this contains a list");
            System.err.println("  of paths separated by newlines (LF).  Paths supplied on the command-line");
            System.err.println("  are ignored when STDIN has data.");
            System.err.println();
            System.err.println("  Sorting the output ( -so / --sortOutput ) might require additional RAM.");
            System.err.println("  To give Java more RAM add the following command-line options to Java:");
            System.err.println();
            System.err.println("      java -Xms1024M -Xmx1024M [...original args, etc...] ");
            System.err.println();
            System.err.println("  The example above gives Java 1GB (1024M) of RAM.");
            System.err.println();
            System.err.println("Enjoy!");
            System.err.println();

            double m = Runtime.getRuntime().maxMemory() / (1024.0 * 1024.0);
            long mem = Math.round(m);
            String mb = "" + mem;
            if (m > 1000.0) {
                long remainder = mem % 1000;
                mem = mem / 1000;
                mb = mem + "," + remainder;
            }
            System.err.println("Java thinks it's allowed to use at most " + mb + "MB of RAM right now.");
            System.exit(1);
            return;
        }

        // Sorting of some kind might be requested in first four command-line args.
        TreeSet<String> set = new TreeSet<String>();
        for (int i = 0; i < 5; i++) {
            if (i < args.length) {
                String arg = args[i];
                if (arg.startsWith("-s") || arg.startsWith("--s")) {
                    set.add(arg);
                }
            }
        }

        boolean useStdIn = set.contains("--stdin");
        boolean sortOutput = set.contains("--sortOutput") || set.contains("-so");
        boolean sortInnerClasses = set.contains("--sortInnerClasses") || set.contains("-si");
        boolean sortMethods = set.contains("--sortMethods") || set.contains("-sm");
        boolean sortFields = set.contains("--sortFields") || set.contains("-sf");
        ScanConfig scanConfig = new ScanConfig(sortInnerClasses, sortMethods, sortFields);

        ArrayList<Artifact> artifacts = new ArrayList<Artifact>(16384);

        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            ArgScanner scanner;
            if (useStdIn) {
                isr = new InputStreamReader(System.in, "UTF-8");
                br = new BufferedReader(isr);
                scanner = new ArgScanner(br);
            } else {
                scanner = new ArgScanner(args);
            }

            String s;
            while ((s = scanner.nextArg()) != null) {
                String S = s.toUpperCase(Locale.ENGLISH);

                // Skip inner classe files.
                if (InnerClass.isChildClass(s)) { continue; }

                // Skip package-info
                if (S.endsWith("PACKAGE-INFO.CLASS") || S.endsWith("PACKAGE-INFO.JAVA")) { continue; }

                File f = null;
                boolean isSrc = false;
                if (S.endsWith(".CLASS") || S.endsWith(".JAVA")) {
                    f = new File(s);
                    isSrc = S.endsWith(".JAVA");
                }
                if (f != null && f.isFile()) {
                    NameAndBytes nab;
                    try {
                        // System.out.println("S;" + f.getPath() + "\n");
                        nab = new NameAndBytes(isSrc, f);
                    } catch (IOException ioe) {
                        System.err.println("E;Failed to load file [" + f + "] - " + ioe);
                        continue;
                    }
                    try {
                        if (isSrc) {
                            // *.java is a little tricky:  might include 'appended' non-public classes.
                            Src src = new Src(nab, scanConfig);
                            artifacts.addAll(src.scan());
                        } else {
                            // *.class is harder:  need to find related inner classes (if applicable).
                            List<NameAndBytes> relatedClasses = new ArrayList<NameAndBytes>();
                            String n = f.getName();
                            String prefix = n.substring(0, n.length() - 6) + "$";

                            // find related inner classes
                            File d = f.getParentFile();
                            File[] siblings = d != null ? d.listFiles() : null;
                            if (siblings != null) {
                                for (File sibling : siblings) {

                                    String name = sibling.getName();
                                    String NAME = sibling.getName().toUpperCase(Locale.ENGLISH);
                                    if (NAME.endsWith(".CLASS") && name.startsWith(prefix)) {

                                        String innerName = name.substring(prefix.length(), name.length() - 6);
                                        int x = innerName.lastIndexOf('$');
                                        if (x >= 0) {
                                            innerName = innerName.substring(x + 1);
                                        }

                                        // Skip anonymous-inner classes (e.g. File$1.class, File$Inner$2.class, etc...)
                                        if (!Numbers.isLong(innerName)) {
                                            try {
                                                relatedClasses.add(new NameAndBytes(NameAndBytes.BINARY, sibling));
                                            } catch (IOException ioe) {
                                                System.err.println("load inner-class [" + name + "] failed: " + ioe);
                                            }
                                        }
                                    }
                                }
                            }
                            Bin bin = new Bin(nab, relatedClasses, scanConfig);
                            artifacts.add(bin);
                        }
                    } catch (Throwable t) {
                        ByteArrayOutputStream bout = new ByteArrayOutputStream();
                        try {
                            PrintStream ps = new PrintStream(bout, false, "UTF-8");
                            t.printStackTrace(ps);
                            ps.flush();
                            ps.close();
                            System.out.println("\nn;" + f.getPath());
                            System.out.println(bout.toString("UTF-8"));
                            System.err.println("Failed on: [" + f.getPath() + "] Reason: " + t);
                            System.exit(1);
                        } catch (UnsupportedEncodingException uee) {
                            // ignore, since UTF-8 is always supported.
                        }
                    }
                }
                if (!sortOutput) {
                    for (Artifact a : artifacts) {
                        System.out.println("\nn;" + a.getPath());
                        System.out.println(a.toTightString().trim());
                    }
                    artifacts.clear();
                }
            }
        } finally {
            Util.close(br, isr);
        }

        if (!sortOutput) {
            if (!artifacts.isEmpty()) {
                System.out.println("ERROR:  artifacts should be empty if we're not sorting!");
            }
        }

        // Sort them so that we can get 'appended' classes back into the right order.
        Collections.sort(artifacts);
        for (Artifact a : artifacts) {
            System.out.println("\nn;" + a.getPath());
            System.out.println(a.toTightString().trim());
        }
    }
}
