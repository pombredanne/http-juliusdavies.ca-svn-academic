package ca.juliusdavies.signature;

import ca.phonelist.util.Bytes;
import ca.phonelist.util.NullSafe;
import org.objectweb.asm.ClassReader;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public class NameAndBytes implements Serializable, Comparable<NameAndBytes> {
    public static final boolean BINARY = false;
    public static final boolean SOURCE = true;

    public final boolean isSrc;
    public final String name;
    public final String path;
    public final long lastModified;
    public final byte[] bytes;
    public transient ClassReader cr; // just a holder to help with inner-classes

    public NameAndBytes(boolean isSrc, File f) throws IOException {
        this(
                isSrc, f.getName(), f.getPath(), f.lastModified(), Bytes.fileToBytes(f)
        );
    }

    public NameAndBytes(boolean isSrc, String name, String path, long lastModified, byte[] bytes) {
        this.isSrc = isSrc;
        this.name = name;
        this.path = path;
        this.lastModified = lastModified;
        this.bytes = bytes;
    }

    public int compareTo(NameAndBytes other) {
        return NullSafe.compare(path, other.path);
    }

}
