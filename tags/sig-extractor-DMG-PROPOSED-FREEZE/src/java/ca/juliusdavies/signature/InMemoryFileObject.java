package ca.juliusdavies.signature;

import javax.tools.*;
import java.io.*;
import java.util.*;
import java.net.*;


public class InMemoryFileObject extends SimpleJavaFileObject {
  private byte[] bytes;
  private long lastModified;
  private String path;
  private String name;

  public InMemoryFileObject(String name, String path, long date, byte[] bytes) throws URISyntaxException {

    super(new URI("file:///dev/null/" +       path.replaceAll(" ", "%20")  + "/" + name), JavaFileObject.Kind.SOURCE);
    this.bytes = bytes;
    this.lastModified = date;
    this.name = name;
    this.path = path;
  }

  public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
    return new String(bytes, 0, bytes.length, "UTF-8");
  }

  public long getLastModified() { return lastModified; }

  public String getName() { return name; }

  public boolean isNameCompatible(String simpleName, JavaFileObject.Kind kind) {
    if (JavaFileObject.Kind.SOURCE.equals(kind)) {
        String s = name.substring(0, Math.min(name.length(), simpleName.length()));
        // System.out.println(name + "] vs. [" + simpleName + "] " +  simpleName.equalsIgnoreCase(s) );
        return name.equalsIgnoreCase(s);
    } else {
        throw new UnsupportedOperationException("don't support that kind: " + kind);
    }
  }

  public InputStream openInputStream() throws IOException { return new ByteArrayInputStream(bytes); }

  public Reader openReader() throws IOException { return new InputStreamReader(openInputStream(), "UTF-8"); }

  public String toString() { return name; }


}
