package ca.juliusdavies.signature;

import ca.phonelist.util.NullSafe;
import ca.phonelist.util.Strings;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static org.objectweb.asm.Opcodes.*;

public class Bin extends Artifact {
    public final Comparator<Bin> BY_TOP_LINE = new Comparator<Bin>(){
        public int compare(Bin b1, Bin b2) {
            return NullSafe.compare(b1.topLine, b2.topLine);
        }
    };

    private static final int TYPE_CLASS = 1;
    private static final int TYPE_METHOD = 2;
    private static final int TYPE_FIELD = 3;
    private static final String JAVA_LANG_OBJECT = "java/lang/Object";
    private static final String JAVA_LANG_ANNOTATION = "java/lang/annotation/Annotation";
    private static final String CONSTRUCTOR_NAME = "<init>";

    /* raw data to help us reassemble the inner classes */
    private transient Map<String,NameAndBytes> allKnownInnerClasses = new TreeMap<String,NameAndBytes>();
    private Bin ultimateParent;

    private int level;
    private String fqn;
    private String name;
    private String pkg;
    private String tightSig;
    private String parent;
    private int myAccess;
    private LinkedHashMap<String, Integer> myInnerClasses = new LinkedHashMap<String, Integer>();
    private StringBuilder thisBuf = new StringBuilder(1024);
    private ScanConfig config = ScanConfig.DEFAULT_CONFIG;

    private String topLine;

    public Bin(File f) throws IOException { this(new NameAndBytes(NameAndBytes.BINARY, f)); }

    public Bin(NameAndBytes nab) { this(nab, null, ScanConfig.DEFAULT_CONFIG); }

    private Bin(NameAndBytes nab, Bin ultimateParent, String parent, int myAccess) {
        this(nab);
        this.ultimateParent = ultimateParent;
        this.myAccess = myAccess;
        this.parent = parent;
        this.config = ultimateParent.config;
    }

    public Bin(NameAndBytes nab, List<NameAndBytes> knownInnerClasses, ScanConfig config) {
        super(nab.name, nab.path, nab.lastModified, nab.bytes);
        this.config = config;
        if (knownInnerClasses != null) {
            this.ultimateParent = this;
            for (NameAndBytes innerNab : knownInnerClasses) {
                innerNab.cr = new ClassReader(innerNab.bytes);
                String fqn = Names.fixClassName(innerNab.cr.getClassName());
                allKnownInnerClasses.put(fqn, innerNab);
            }
            scan(new ClassReader(bytes));

            // only outter class needs a path
            this.path = path + ";" + this.name;
        } else {
            // postpone scan for inner-classes... we need more info
        }
    }

    public static void main(String[] args) throws IOException {
        File f = new File(args[0]);
        NameAndBytes nab = new NameAndBytes(NameAndBytes.BINARY, f);
        Bin b = new Bin(nab);
        System.out.println(b.toTightString());
    }

    private void initSignature() {
        if (thisBuf != null) {
            try {
                assembleSignature(0);
            } catch (MissingInnerClassException mice) {
                this.tightSig = "E;-1;Failed to parse [" + fqn + "]. Cannot find inner-class bytecode: [" + mice.fqn + "]";
            }
        }
    }

    private void assembleSignature(int level) {
        if (thisBuf != null) {
            StringBuilder innerClassBuf = new StringBuilder();

            ArrayList<String> innerClasses = new ArrayList<String>(myInnerClasses.keySet());

            // For some reason asm.jar lists inner-classes in reverse order.
            Collections.reverse(innerClasses);

            ArrayList<Bin> inners = new ArrayList<Bin>();

            for (String innerFQN : innerClasses) {
                Integer innerAccess = myInnerClasses.get(innerFQN);
                NameAndBytes nab = ultimateParent.allKnownInnerClasses.get(innerFQN);
                if (nab == null) {
                    throw new MissingInnerClassException(innerFQN);
                }
                ClassReader cr = nab.cr;
                Bin b = new Bin(nab, ultimateParent, this.name, innerAccess);
                b.scan(cr);
                b.assembleSignature(level + 1);
                inners.add(b);
            }

            if (config.sortInnerClasses) {
                Collections.sort(inners, BY_TOP_LINE);
            }

            for (Bin b : inners) {
                String tightString = b.toTightString();
                innerClassBuf.append(tightString);
            }

            thisBuf.append(innerClassBuf);
            tightSig = thisBuf.toString();
            if (level == 0) {
                tightSig = tightSig.trim();
            }
            thisBuf = null;
        }
    }

    public String getPackageName() { return pkg; }
    public String getClassName() { return name; }
    public String toTightString() { initSignature(); return tightSig; }
    public String toLooseString() { return ""; }
    public int getMinLine() { return -1; }
    public int getMaxLine() { return -1; }
    public int getMethodCount() { return -1; }

    private void scan(ClassReader cr) {
        int flags = ClassReader.SKIP_FRAMES | ClassReader.SKIP_CODE | ClassReader.SKIP_DEBUG;
        cr.accept(this, flags);
    }

    public void visit(int version, int access, String name, String sig, String superName, String[] interfaces) {
        boolean isInnerClass = this != ultimateParent;
        if (!isInnerClass) {
            this.myAccess = access;
        }

        int x = name.lastIndexOf('/');
        this.level = Strings.countChar(name.substring(x >= 0 ? x : 0), '$');
        this.fqn = Names.fixClassName(name);
        x = fqn.lastIndexOf('.');
        if (x >= 0) {
            this.pkg = fqn.substring(0, x);
            this.name = fqn.substring(x + 1);
        } else {
            this.pkg = "";
            this.name = fqn;
        }

        String genericExtendsString = Names.fixTypeName(superName);
        String genericImplementsString = null;
        String generic = null;
        if (sig != null) {
            sig = sig.replace("::", ":");
            // System.out.println("SIG: [" + sig + "]");

            if (sig.charAt(0) == '<') {
                x = findClosingBracket(sig, 1);
                generic = sig.substring(0, x+1);
                sig = sig.substring(x+1);
            }
            generic = genericParser(generic);
            sig = signatureParser(sig);
            if (interfaces != null && interfaces.length > 0) {
                x = firstComma(sig, 0);
                genericExtendsString = sig.substring(0, x);
                genericImplementsString = sig.substring(x+1);
            } else {
                genericExtendsString = sig;
            }
        }

        // Interfaces are always abstract and static in bytecode, but source
        // representation doesn't bother specifying this redundant info.
        if (is(ACC_INTERFACE, myAccess)) {
            myAccess = myAccess & ~ACC_ABSTRACT & ~ACC_STATIC;
        }
        // enum's are always final, static in bytecode, but source
        // representation doesn't bother mentioning this.
        if (is(ACC_ENUM, myAccess)) {
            myAccess = myAccess & ~ACC_FINAL & ~ACC_STATIC;
        }

        thisBuf.append("\nc;").append(level).append(";");
        for (int i = 0; i < level; i++) {
            thisBuf.append("  ");
        }
        thisBuf.append(extractModifiers(TYPE_CLASS, myAccess));
        thisBuf.append(is(ACC_INTERFACE, myAccess) ? "interface " : "class ");
        thisBuf.append(isInnerClass ? Names.fixTypeName(fqn) : fqn);
        if (generic != null) {
            thisBuf.append(generic);
        }
        if (!JAVA_LANG_OBJECT.equals(superName) && !is(ACC_ENUM, myAccess)) {
            thisBuf.append(" extends ").append(genericExtendsString);
        }

        // Weird quirk:  interfaces CANNOT implement, but that's
        // how the @interface tag is dealt with.  Not visible to our
        // source processor, so we need to omit this.
        if (interfaces != null && interfaces.length == 1) {
            if (JAVA_LANG_ANNOTATION.equals(interfaces[0]) && is(ACC_INTERFACE, myAccess)) {
                interfaces = null;
            }
        }

        if (interfaces != null && interfaces.length > 0) {
            thisBuf.append(" implements ");
            if (genericImplementsString != null) {
                // The generic "implements" clause (when applicable).
                thisBuf.append(genericImplementsString);
            } else {
                // Non-generic version.
                for (String s : interfaces) {
                    thisBuf.append(Names.fixTypeName(s)).append(",");
                }
                // Delete final comma.
                thisBuf.deleteCharAt(thisBuf.length()-1);
            }
        }

        this.topLine = thisBuf.toString().trim();
    }

    public void visitInnerClass(String fullName, String outerName, String innerName, int access) {
        if (outerName != null && innerName != null && fqn.equals(Names.fixClassName(outerName))) {
            myInnerClasses.put(fqn + "." + innerName, access);
        }
    }

    public FieldVisitor visitField(int access, String name, String desc, String sig, Object value) {
        if (is(ACC_SYNTHETIC, access)) {
            return null;
        }
        thisBuf.append("\nf;").append(level).append(";");
        for (int i = 0; i <= level; i++) {
            thisBuf.append("  ");
        }
        thisBuf.append(extractModifiers(TYPE_FIELD, access));
        if (sig != null) {
            thisBuf.append(signatureParser(sig));
        } else {
            String type = Names.fixTypeName(Type.getType(desc).getClassName());
            thisBuf.append(type);
        }
        thisBuf.append(" ").append(name).append(';');
        return null;
    }

    public MethodVisitor visitMethod(int access, String name, String desc, String sig, String[] exceptions) {

        if (is(ACC_SYNTHETIC, access) || is(ACC_BRIDGE, access)) {
            return null;
        }

        // for some reason our source parser cannot see static initializers
        if ("<clinit>".equals(name)) {
            return null;
        }

        // javac inserts "values()" and "valueOf(String)" methods into enum's,
        // but our source parser cannot see them, so we must watch out for them.
        if (is(ACC_ENUM, myAccess) && is(ACC_STATIC, access)) {
            if ("values".equals(name) && desc != null && desc.startsWith("()")) {
                return null;
            }
            if ("valueOf".equals(name) && desc != null && desc.startsWith("(Ljava/lang/String;)")) {
                return null;
            }
        }

        // javac inserts "private <init>(String,int)" into enum's,
        // but we are having some problems with these.
        if (is(ACC_ENUM, myAccess) && is(ACC_PRIVATE, access)) {
            if ("<init>".equals(name) && desc != null && desc.startsWith("()")) {
                return null;
            }
            if ("<init>".equals(name) && desc != null && desc.startsWith("(Ljava/lang/String;I)")) {
                return null;
            }
        }

        thisBuf.append("\nm;").append(level).append(";");
        for (int i = 0; i < level; i++) {
            thisBuf.append("  ");
        }
        thisBuf.append("  ");

        // If the class is strictfp, it's redundant to mark the method as strictfp.
        if (is(ACC_STRICT, myAccess)) {
            access = access & ~ACC_STRICT;
        }

        // If the class is an interface, it's redundant to mark the method as abstract.
        if (is(ACC_INTERFACE, myAccess)) {
            access = access & ~ACC_ABSTRACT;
        }

        thisBuf.append(extractModifiers(TYPE_METHOD, access));

        if (sig == null) {
            sig = desc;
        }
        String returnSig = "";
        String methodSig = sig;
        if (sig != null) {
            int x = sig.indexOf('(');
            int y = sig.indexOf(')');
            if (x >= 0) {
                methodSig = sig.substring(x + 1, y);
                returnSig = sig.substring(y + 1);
            }

            returnSig = signatureParser(returnSig);
            methodSig = signatureParser(methodSig);
        }

        if (!CONSTRUCTOR_NAME.equals(name)) {
            thisBuf.append(returnSig).append(" ");
        }
        thisBuf.append(name).append("(");

        if ("<init>".equals(name) && ultimateParent != this && !is(ACC_STATIC, myAccess) && methodSig.startsWith(parent)) {
            methodSig = methodSig.substring(parent.length());
            if (!"".equals(methodSig) && methodSig.charAt(0) == ',') {
                methodSig = methodSig.substring(1);
            }
        }

        thisBuf.append(methodSig);
        thisBuf.append(")");

        if (exceptions != null && exceptions.length > 0) {
            thisBuf.append(" throws ");
            for (String x : exceptions) {
                thisBuf.append(Names.fixTypeName(x)).append(',');
            }
            // Delete the final comma.
            thisBuf.deleteCharAt(thisBuf.length()-1);
        }

        return null;
    }

    public static String extractModifiers(int type, int access) {
        // ignore:
        // ACC_SUPER
        // ACC_VARARGS
        // ACC_INTERFACE

        // Also ignore: DEPRECATED
        // ( deprecated doesn't seem to showup during the phases of javac
        //   we are invoking in the *.java scanner ).

        StringBuilder buf = new StringBuilder();
        if (is(ACC_SYNTHETIC, access)) buf.append("synthetic ");
        if (type != TYPE_FIELD && is(ACC_BRIDGE, access)) buf.append("bridge ");
        if (is(ACC_PUBLIC, access)) buf.append("public ");
        if (is(ACC_PROTECTED, access)) buf.append("protected ");
        if (is(ACC_PRIVATE, access)) buf.append("private ");
        if (is(ACC_FINAL, access)) buf.append("final ");
        if (is(ACC_ABSTRACT, access)) buf.append("abstract ");
        if (is(ACC_STATIC, access)) buf.append("static ");
        if (is(ACC_NATIVE, access)) buf.append("native ");
        if (type == TYPE_METHOD && is(ACC_SYNCHRONIZED, access)) buf.append("synchronized ");
        if (is(ACC_ENUM, access)) buf.append("enum ");
        // if (type == TYPE_METHOD && is(ACC_DEPRECATED, access)) buf.append("deprecated ");
        if (is(ACC_STRICT, access)) buf.append("strictfp ");
        if (type == TYPE_FIELD && is(ACC_TRANSIENT, access)) buf.append("transient ");
        if (type == TYPE_FIELD && is(ACC_VOLATILE, access)) buf.append("volatile ");
        return buf.toString();
    }

    private static strictfp boolean is(int flag, int var) { return (flag & var) != 0; }

    private static String genericParser(String generic) {
        if (generic != null) {
            StringBuilder buf = new StringBuilder();
            generic = generic.substring(1, generic.length()-1);
            String[] toks = generic.split(":");
            for (int i = 0; i < toks.length; i++) {
                String t = toks[i];
                int x = t.lastIndexOf(';');
                if (x > 0) {
                    toks[i-1] = toks[i-1] + ":" + t.substring(0, x+1);
                    toks[i] = t.substring(x+1);
                }
            }

            buf.append('<');
            for (String t : toks) {
                if ("".equals(t)) {
                    continue;
                }
                int x = t.indexOf(':');
                String sig = t.substring(x+1);

                String orig = sig;
                sig = signatureParser(sig);

// System.out.println("GEN: [" + orig + "] / [" + sig + "]");


                t = x >= 0 ? t.substring(0,x) : t;
                if ("Object".equals(sig)) {
                    buf.append(t).append(',');
                } else {
                    buf.append(t).append(" extends ").append(sig).append(',');
                }
            }
            buf.setCharAt(buf.length()-1, '>');
            return buf.toString();
        }
        return null;
    }

    private static String signatureParser(String sig) {
        if (sig == null || "".equals(sig)) {
            return "";
        }

        if (!sig.endsWith(";")) {
            sig = sig + ";";
        }
        sig = sig.replace(";>", ">");
        sig = sig.replace(">", ";>");

        StringBuilder buf = new StringBuilder(sig.length());
        signatureParser(sig, 0, buf);
        return buf.toString();
    }

    private static int findClosingBracket(String sig, int pos) {
        for (int i = pos; i < sig.length(); i++) {
            char c = sig.charAt(i);
            if (c == '<') {
                i = findClosingBracket(sig, i+1);
            } else if (c == '>') {
                return i;
            }
        }
        throw new RuntimeException("Could not findClosingBracket: [" + sig + "] / pos=" + pos);
    }

    private static int firstComma(String sig, int pos) {
        for (int i = pos; i < sig.length(); i++) {
            char c = sig.charAt(i);
            if (c == '<') {
                i = findClosingBracket(sig, i+1);
            } else if (c == ',') {
                return i;
            }
        }
        throw new RuntimeException("Could not find firstComma: [" + sig + "]");
    }

    private static int signatureParser(String sig, int pos, StringBuilder buf) {
        for (int i = pos; i < sig.length(); i++) {
            char c = sig.charAt(i);
            switch (c) {
                case ';': {
                    int bracketCount = toJavaType(sig.substring(pos, i), buf);
                    appendBrackets(bracketCount, buf);
                    if (sig.length() > i + 1 && sig.charAt(i + 1) != '>') {
                        buf.append(',');
                    }
                    pos = i;
                }
                break;
                case '<': {
                    int bracketCount = toJavaType(sig.substring(pos, i), buf);
                    buf.append('<');
                    i = signatureParser(sig, i+1, buf);
                    buf.append('>');
                    appendBrackets(bracketCount, buf);
                    pos = i;
                }
                break;
                case '>':
                    return i;
            }
        }

        return sig.length();
    }

    private static int toJavaType(String t, StringBuilder buf) {
        boolean isFirstToken = true;
        int bracketCount = 0;
        boolean hasGenericWildcard = false;
        boolean hasGenericSuper = false;
        for (int i = 0; i < t.length(); i++) {
            char c = t.charAt(i);
            String tok = null;
            switch (c) {
                case '[':
                    bracketCount++;
                    break;
                case '*':
                    tok = "?";
                    break;
                case 'T':
                    int x = t.indexOf(';', i+2);
                    x = x < 0 ? t.length() : x;
                    tok = t.substring(i+1, x);
                    i = x+1;
                    break;
                case 'Z':
                    tok = "boolean";
                    break;
                case 'B':
                    tok = "byte";
                    break;
                case 'S':
                    tok = "short";
                    break;
                case 'C':
                    tok = "char";
                    break;
                case 'I':
                    tok = "int";
                    break;
                case 'J':
                    tok = "long";
                    break;
                case 'F':
                    tok = "float";
                    break;
                case 'D':
                    tok = "double";
                    break;
                case 'V':
                    tok = "void";
                    break;
                case '+':
                    hasGenericWildcard = true;
                    break;
                case '-':
                    hasGenericSuper = true;
                    break;
                case 'L':
                    x = t.indexOf(';', i);
                    if (x < 0) {
                        x = t.length();
                    }

                    tok = t.substring(i + 1, x);
                    tok = Names.fixTypeName(tok);
                    i = x+1;

                default:
                    break;
            }

            if (tok != null) {
                if (hasGenericWildcard) {
                    buf.append("? extends ");
                }
                if (hasGenericSuper) {
                    buf.append("? super ");
                }
                if (!isFirstToken) {
                    buf.append(',');
                }
                isFirstToken = false;
                buf.append(tok);

                if (i < t.length()) {
                    appendBrackets(bracketCount, buf);
                    bracketCount = 0;
                }
            }

        }
        return bracketCount;
    }

    private static void appendBrackets(int bracketCount, StringBuilder buf) {
        for (int j = 0; j < bracketCount; j++) {
            buf.append("[]");
        }
    }

    public String toString() { return getFQN(); }

}
