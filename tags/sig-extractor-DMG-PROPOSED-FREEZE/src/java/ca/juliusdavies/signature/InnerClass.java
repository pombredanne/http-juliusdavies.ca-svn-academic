package ca.juliusdavies.signature;

import java.util.zip.*;
import java.io.*;
import java.util.*;



public class InnerClass {

  public static boolean isChildClass(String name) {
    return name.endsWith(".class") && name.indexOf('$') >= 0;
  }

  public static String ultimateParent(String child) {
    int x = child.indexOf('$');
    if (x >= 0) {
      return child.substring(0, x) + ".class";
    } else {
      return null;
    }
  }

  public static String immediateParent(String child) {
    int x = child.lastIndexOf('$');
    if (x >= 0) {
      return child.substring(0, x) + ".class";    
    } else {
      return null;
    }
  }
  
  
  public static void main(String[] args) throws Exception {
    File f = new File(args[0]);
    scan(f);
  }
  
  private static void scan(File file) throws Exception {
    if (file.isDirectory()) {
      for (File f : file.listFiles()) {
        scan(f);
      }
    } else if (file.isFile()) {
      String name = file.getName();      
      if (isChildClass(name)) {
        System.out.println(ultimateParent(name) + "]\t[" + immediateParent(name) + "]\t[" + name + "]");
      }
    }
  }

}