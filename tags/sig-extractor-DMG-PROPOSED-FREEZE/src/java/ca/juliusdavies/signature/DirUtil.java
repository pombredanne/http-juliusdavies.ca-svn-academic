package ca.juliusdavies.signature;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;


public class DirUtil {

  public static List<Bin> getClasses(File dir) throws Exception {
    ArrayList<Bin> list = new ArrayList<Bin>(); 
    TreeMap<String, List<NameAndBytes>> parentToInner = new TreeMap<String, List<NameAndBytes>>();

    for (File f : dir.listFiles()) {
      String name = f.getName();
      if (InnerClass.isChildClass(name)) {
        String k = InnerClass.ultimateParent(name);
        List<NameAndBytes> v = parentToInner.get(k);
        if (v == null) {
          v = new ArrayList<NameAndBytes>();
          parentToInner.put(k, v);
        }
        v.add(new NameAndBytes(NameAndBytes.BINARY, f));
      }
    }
    
    for (File f : dir.listFiles()) {
      String name = f.getName();
      if (name.endsWith(".class") && !InnerClass.isChildClass(name)) {
        list.add(new Bin(new NameAndBytes(NameAndBytes.BINARY, f), parentToInner.get(name), ScanConfig.DEFAULT_CONFIG));
      }
    }
        
    return list;
  }
  
  
  public static void main(String[] args) throws Exception {
    File dir = new File(args[0]);
    List<Bin> list = getClasses(dir);
    for (Bin b : list) {
      System.out.println(b.toTightString());
      System.out.println();
    }
  }
}