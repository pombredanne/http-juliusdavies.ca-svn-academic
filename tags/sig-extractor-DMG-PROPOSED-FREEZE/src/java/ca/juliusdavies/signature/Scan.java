package ca.juliusdavies.signature;

import ca.phonelist.util.Base64;
import ca.phonelist.util.Bytes;
import ca.phonelist.util.Util;
import ca.phonelist.util.Dates;
import ca.phonelist.util.Hex;

import java.io.*;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author Julius Davies
 * @since Apr 18, 2010
 */
public class Scan {

    public static FileOutputStream SRC_SQL_F = null;
    public static FileOutputStream BIN_SQL_F = null;

    public static PrintStream SRC_SQL = null;
    public static PrintStream BIN_SQL = null;

    public final static DateFormat DF = new SimpleDateFormat("yyyy-MM-dd/HH:mm:ss");
    private final static MessageDigest SHA1;
    private static int[] ninkaCount = {0,0,0};
    private static int scanCount = 0;

    private static boolean OUTPUT_QUERY_DB_NATURAL = false;
    private static boolean OUTPUT_QUERY_DB_CROSS = false;
    private static boolean OUTPUT_SIGS_TUPLES = false;
    private static boolean OUTPUT_SIGS = false;
    private static boolean TIGHT = true;
    private static String TO_LOOK_FOR = null;

    private final static Map<Thread, Integer> THREAD_IDS = new ConcurrentHashMap<Thread, Integer>();
    private final static LinkedBlockingQueue<Artifact> IN_FLIGHT = new LinkedBlockingQueue<Artifact>(3);
    private final static ExecutorService EXECUTOR = Executors.newFixedThreadPool(IN_FLIGHT.remainingCapacity());

    private static String VISITED_PATH = null;
    private static boolean ABOUT_TO_CHANGE_SKIP_MODE = false;
    private static boolean SKIP_MODE = false;
    private static long SKIP_COUNT = 0;

    static {
        DF.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            SRC_SQL_F = new FileOutputStream("jarchive.src.sql", false);
            BIN_SQL_F = new FileOutputStream("jarchive.bin.sql", false);
            SRC_SQL = new PrintStream(SRC_SQL_F, true, "UTF-8");
            BIN_SQL = new PrintStream(BIN_SQL_F, true, "UTF-8");
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }

        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA1");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        SHA1 = md;

    }

    public static void main(String[] args) throws Exception {

        File f = new File("partial");

        if (f.exists()) {
            FileInputStream fin = new FileInputStream("partial");
            InputStreamReader r = new InputStreamReader(fin, "UTF-8");
            BufferedReader br = new BufferedReader(r);
            String line = br.readLine();
            while (line != null) {
                String[] toks = line.split("\t");
                if (toks.length > 8) {
                    VISITED_PATH = toks[7];
                }
                line = br.readLine();
            }
            br.close();
            r.close();
            fin.close();
        }
        if (VISITED_PATH != null) {
            SKIP_MODE = true;
        }


        System.out.println(Dates.now() + " Starting!");
        String arg0 = args[0];
        OUTPUT_QUERY_DB_NATURAL = arg0.startsWith("qn");
        OUTPUT_QUERY_DB_CROSS = arg0.startsWith("qx");

        if (arg0.endsWith("l")) {
            TIGHT = false;
        }

        OUTPUT_SIGS = arg0.startsWith("s");
        if (arg0.startsWith("st")) {
            OUTPUT_SIGS_TUPLES = true;
            OUTPUT_SIGS = false;
        }

        ArrayList<Artifact> binList = new ArrayList<Artifact>(4096);
        ArrayList<Artifact> srcList = new ArrayList<Artifact>(4096);
        ArrayList<Artifact>[] lists = new ArrayList[]{binList, srcList};

        String toLookFor = args[1];
        boolean binOnly = "bin".equals(toLookFor);
        boolean srcOnly = "src".equals(toLookFor);
        if (!binOnly && !srcOnly) {
            TO_LOOK_FOR = toLookFor.trim();
        }

        if (srcOnly) {
            lists[0] = null;
        } else if (binOnly) {
            lists[1] = null;
        } else {
            lists[0] = null;
            lists[1] = null;
        }

        long start = System.currentTimeMillis();
        try {
            for (int i = 2; i < args.length; i++) {
                File d = new File(args[i]);
                if (d.getName().endsWith(".txt")) {
                    FileInputStream fin = null;
                    InputStreamReader isr = null;
                    BufferedReader br = null;
                    try {
                        fin = new FileInputStream(d);
                        isr = new InputStreamReader(fin);
                        br = new BufferedReader(isr);
                        String line = br.readLine();
                        while (line != null) {
                            line = line.trim();
                            System.out.println("Scanning: [" + line + "]");
                            f = new File(line);
                            if (f.exists()) {
                                Scan.scan(f, lists);
                            }
                            line = br.readLine();
                        }
                    } finally {
                        Util.close(br, isr, fin);
                    }
                } else {
                    Scan.scan(d, lists);
                }
            }

            for (ArrayList<Artifact> list : lists) {
                if (list != null && !list.isEmpty()) {

                    StringBuilder buf = null;

                    for (Artifact a : list) {
                        if (OUTPUT_QUERY_DB_NATURAL || OUTPUT_QUERY_DB_CROSS || OUTPUT_SIGS || OUTPUT_SIGS_TUPLES) {
                            // do nothing
                        } else {
                            insert(a);                            
                        }
                    }
                }
            }

        } finally {
            Util.close(SRC_SQL, BIN_SQL, SRC_SQL_F, BIN_SQL_F);
        }

        long end = System.currentTimeMillis();
        // System.out.println(" took " + (end - start) + "ms");

        if (binList.size() == 1) {
            System.out.println(binList);
        }

        if (srcList.size() == 1) {
            System.out.println(srcList);
        }

        System.out.println(Dates.now() + " Done!");
        EXECUTOR.shutdown();
    }

    private static void clean(ArrayList<Artifact>[] lists) {
        for (ArrayList<Artifact> list : lists) {
            if (list != null && list.size() > 100) {
                if (OUTPUT_QUERY_DB_NATURAL || OUTPUT_QUERY_DB_CROSS || OUTPUT_SIGS_TUPLES) {
                    if (list.size() % 1000 == 0) {
                        System.err.println(Dates.now() + " " + list.size());
                    }
                } else if (!OUTPUT_SIGS) {
                    for (Artifact a : list) {
                        if (scanCount % 1000 == 0) {
                            System.err.println(Dates.now() + " " + scanCount);
                        }
                        insert(a);
                    }
                    list.clear();
                }
            }
        }
    }

    private static void add(Artifact a, ArrayList<Artifact>[] lists) {
        if (a == null) {
            return;
        }
        if (TO_LOOK_FOR != null) {
            if (TO_LOOK_FOR.equals(a.getFQN())) {
                System.out.println(a.getFileName());
                System.out.println(a);
            }
            return;
        }
        if (a instanceof Bin) {
            lists[0].add(a);
        } else {
            lists[1].add(a);
        }
    }

    public static void scan(File d, ArrayList<Artifact>[] lists) throws IOException {
        clean(lists);

        if (d.isDirectory()) {
            File[] files = d.listFiles();
            if (files != null) {
                for (File f : files) {
                    scan(f, lists);
                }
            }
        } else {

            StringBuilder buf = null;

            String path = d.getCanonicalPath();
            if (SKIP_MODE) {
                SKIP_COUNT++;
                if (SKIP_COUNT % 10000 == 0) {
                    System.out.println(SKIP_COUNT + " [" + path + "]");
                }
                if (VISITED_PATH.equals(path)) {
                    ABOUT_TO_CHANGE_SKIP_MODE = true;
                    System.out.println(SKIP_COUNT + " changing... [" + path + "]");                    
                } else if (ABOUT_TO_CHANGE_SKIP_MODE) {
                    SKIP_MODE = false;
                    System.out.println(SKIP_COUNT + " done!!!     [" + path + "]");                    
                }

                return;
            }
            String n = d.getName().toLowerCase(Locale.ENGLISH);
            Artifact a = null;

            boolean isFile = n.endsWith(".java") || n.endsWith(".class");
            boolean isZip = n.endsWith(".zip") || n.endsWith(".jar");

            if (isFile && n.indexOf('-') >= 0) {
                // Skip names with dashes in them.  Invalid for java.
                return;
            }
            try {
                if (n.endsWith(".java")) {
                    if (TO_LOOK_FOR != null || lists[1] != null) {
                        a = new Src(d);
                    }
                } else if (n.endsWith(".class")) {
                    if (TO_LOOK_FOR != null || lists[0] != null) {
                        a = new Bin(d);
                    }
                }
            } catch (Exception e) {
                System.out.println("file err: " + d + " " + e);
            }

            add(a, lists);
            if (isZip) {
                FileInputStream fin = null;
                ZipInputStream zin = null;
                try {
                    fin = new FileInputStream(d);
                    zin = new ZipInputStream(fin);
                    scanZip(path, zin, lists);
                } finally {
                    Util.close(zin, fin);
                }
            }

            if (OUTPUT_QUERY_DB_NATURAL || OUTPUT_QUERY_DB_CROSS || OUTPUT_SIGS || OUTPUT_SIGS_TUPLES) {
                ArrayList<Artifact> list = lists[0] != null ? lists[0] : lists[1];
                if (list != null && !list.isEmpty()) {
                    buf = new StringBuilder(256);

                    if (OUTPUT_QUERY_DB_NATURAL || OUTPUT_QUERY_DB_CROSS) {

                        boolean isSrcQuery = list.get(0) instanceof Src;
                        if (OUTPUT_QUERY_DB_CROSS) {
                            isSrcQuery = !isSrcQuery;
                        }

                        buf.append("SELECT ");
                        buf.append("\n  t.a AS \"").append(n).append(" = a = ").append(list.size()).append("\", z.zipcount AS b, a_intersect_b, t.a + z.zipcount - a_intersect_b AS a_union_b,");
                        buf.append("\n  to_char(1.0 * a_intersect_b / (t.a + z.zipcount - a_intersect_b), '0.999') AS jaccard,");
                        buf.append("\n  to_char(1.0 * a_intersect_b / t.a, '0.999') AS inclusion,");
                        buf.append("\n  zip_time, indexed_jar, path ");
                        if (isSrcQuery) {
                            buf.append(", minLic, maxLic, distinctLic " );
                        }

                        buf.append("\n  FROM ( ");
                        buf.append("\n    SELECT ");
                        buf.append("\n      ").append(list.size()).append(" AS a, COUNT(DISTINCT pkg || name) AS a_intersect_b, MAX(date) AS zip_time, folder AS indexed_jar, path ");
                        if (isSrcQuery) {
                            buf.append(", \n  MIN(shortLic) AS minLic, MAX(shortLic) AS maxLic, COUNT(DISTINCT shortLic) AS distinctLic ");
                        }
                        buf.append("\n    FROM  ").append(isSrcQuery ? " java " : " class ").append(" j ");
                        buf.append("\n    WHERE ").append(TIGHT ? " tightsig " : " loosesig ").append(" IN ( ");
                        
                        int i = 0;
                        for (; i < list.size(); i++) {
                            a = list.get(i);
                            buf.append("'").append(getSHA1ClassSig(a, true)).append("',");
                            if (i % 10 == 0) {
                                buf.append('\n');
                            }
                        }
                        // remove final comma
                        if ((i - 1) % 10 == 0) {
                            buf.setCharAt(buf.length() - 2, ' ');
                        }
                        buf.setCharAt(buf.length() - 1, ' ');

                        buf.append("\n ) GROUP BY path, folder \n ");
                        buf.append(" ) t INNER JOIN ");
                        buf.append(isSrcQuery ? "zip_match" : "jar_match");
                        buf.append(" z ON (t.path = z.zip) ORDER BY jaccard DESC, inclusion DESC, zip_time ASC ; \n");

                    } else if (OUTPUT_SIGS_TUPLES) {

                        Collections.sort(list);
                        for (Artifact artifact : list) {
                            buf.append(artifact.getFolder());
                            buf.append(";");
                            buf.append(artifact.getPackageName());                            
                            buf.append(";");
                            buf.append(artifact.getClassName());
                            buf.append(";");
                            buf.append(getSHA1ClassSig(artifact, true));
                            buf.append(";");
                            buf.append(getArtifactSha1(artifact));
                            buf.append("\n");
                        }


                    } else {

                        Collections.sort(list);
                        for (Artifact artifact : list) {
                            buf.append(artifact.toTightString());
                            buf.append("\n");
                        }

                    }
                    FileOutputStream fout = null;
                    try {
                        boolean sql = OUTPUT_QUERY_DB_CROSS || OUTPUT_QUERY_DB_NATURAL;
                        String sqlPath = d.getCanonicalPath() + (sql ? ".sql" : ".txt");
                        fout = new FileOutputStream(sqlPath);
                        fout.write(buf.toString().getBytes("UTF-8"));
                        System.out.println("Wrote: [" + sqlPath + "]");
                    } finally {
                        Util.close(fout);
                    }
                }
                list.clear();
            }

        }
    }

    private static void scanZipEntry(
            String path, final ZipInputStream zin, final ZipEntry ze, ArrayList<Artifact>[] lists
    ) throws IOException {
        if (!ze.isDirectory()) {

            String n = ze.getName().trim().toLowerCase(Locale.ENGLISH);
            int x = n.lastIndexOf('/');
            if (x >= 0) {
                n = n.substring(x + 1);
            }

            boolean isClass = n.endsWith(".class");
            boolean isSrc = n.endsWith(".java");
            boolean isZip = n.endsWith(".jar") || n.endsWith(".zip");

            if (isZip) {

                byte[] zip = Bytes.streamToBytes(zin, false);
                InputStream in = new ByteArrayInputStream(zip);
                ZipInputStream newZ = null;
                try {
                    newZ = new ZipInputStream(in);
                    scanZip(path + "!" + ze.getName(), newZ, lists);
                } finally {
                    Util.close(newZ);
                }

            }

            if (TO_LOOK_FOR == null) {
                if (isSrc && lists[1] == null) {
                    return;
                } else if (isClass && lists[0] == null) {
                    return;
                }
            }

            // Dashes in a name aren't going to work for us.
            if (n.indexOf('-') >= 0) {
                return;
            }

            if (isClass || isSrc) {

                if (SKIP_MODE) {
                    SKIP_COUNT++;
                    if (SKIP_COUNT % 10000 == 0) {
                        System.out.println(SKIP_COUNT + " [" + path + "]");
                    }
                    if (VISITED_PATH.equals(path)) {
                        ABOUT_TO_CHANGE_SKIP_MODE = true;
                        System.out.println(SKIP_COUNT + " changing... [" + path + "]");
                    } else if (ABOUT_TO_CHANGE_SKIP_MODE) {
                        SKIP_MODE = false;
                        System.out.println(SKIP_COUNT + " done!!!     [" + path + "]");
                    }
                    return;
                }

                n = ze.getName();
                x = n.lastIndexOf('/');
                if (x >= 0) {
                    n = n.substring(x + 1);
                }

                long time = ze.getTime();

                byte[] zip = Bytes.streamToBytes(zin, false);
                NameAndBytes nab = new NameAndBytes(isSrc, n, path, time, zip);
                Artifact a = isSrc ? new Bin(nab) : new Src(nab);
                add(a, lists);
            }
        }
    }

    private static void scanZip(String path, final ZipInputStream zin, ArrayList<Artifact>[] lists) {
        try {
            ZipEntry ze = zin.getNextEntry();
            while (ze != null) {
                clean(lists);
                scanZipEntry(path, zin, ze, lists);
                ze = zin.getNextEntry();
            }
        } catch (Throwable t) {
            System.out.println("zip err: " + path + " " + t);
            t.printStackTrace(System.out);
            if (t instanceof Error) {
                throw (Error) t;
            }
        }
    }

    private static String getArtifactSha1(Artifact a) {
        byte[] sig;
        synchronized (SHA1) {
            sig = SHA1.digest(a.bytes);
        }
        return Hex.encode(sig);
    }
    
    public static String getSHA1ClassSig(Artifact a, boolean tight) {
        String classSignature = tight ? a.toTightString() : a.toLooseString();
        try {
            byte[] B = classSignature.getBytes("UTF-8");
            byte[] sig;
            synchronized (SHA1) {
                sig = SHA1.digest(B);
            }
            return Base64.encode(sig);
        } catch (IOException ioe) {
            throw new RuntimeException("UTF-8 not avail");
        }
    }

    private static void insert(Artifact a) {
        if (scanCount % 10000 == 0) {
            System.gc();
        }
        scanCount++;

        try {
            IN_FLIGHT.put(a);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(ie);
        }
        EXECUTOR.submit(new NinkaJob(a));
    }

    private static void insertFromThread(Artifact a, int threadId) {
        if (a instanceof Src) {
            return;
        }
        try {

            String name = a.getClassName();
            String pkg = a.getPackageName();
            String ver = a.getVersion();
            Date d = a.getDate();
            String folder = a.getFolder();
            String path = a.getPath();
            int methodCount = a.getMethodCount();
            int minLine = a.getMinLine();
            int maxLine = a.getMaxLine();
            long size = a.getSize();

            String lic = "";

            long now = System.currentTimeMillis();
            byte[] bytes = a.getBytes();
            if (bytes != null) {
                File tmp = new File("/var/tmp/jarchive/thread_" + threadId + "/" + name + ".java");
                FileOutputStream fout = null;
                Process p1 = null;
                Process p2 = null;
                try {
                    fout = new FileOutputStream(tmp, false);
                    fout.write(bytes);
                    fout.close();
                    fout = null;

                    String[] cmd = {"/dev/shm/perl", "/opt/ninka/ninka.pl", tmp.getCanonicalPath()};
                    p1 = Runtime.getRuntime().exec(cmd);
                    InputStream in = p1.getInputStream();
                    byte[] response = Bytes.streamToBytes(in);
                    in.close();
                    String ninkaResponse = new String(response, "UTF-8");
                    int exitCode = p1.waitFor();
                    if (exitCode == 0) {
                        p1.destroy();
                        p1 = null;
                        if (ninkaCount[threadId] % 100 == 0) {
                            String rmCmd = "rm -f /var/tmp/jarchive/thread_" + threadId + "/*.java*";
                            cmd = new String[]{"/bin/bash", "-c", rmCmd};
                            p2 = Runtime.getRuntime().exec(cmd);
                            int code = p2.waitFor();
                            System.out.println(Dates.now() + " " + rmCmd + " exec=" + code);
                        }
                        ninkaCount[threadId]++;

                        lic = ninkaResponse;
                    } else {
                        throw new RuntimeException("Ninka returned: " + exitCode);
                    }
                } catch (Exception e) {
                    System.out.println("Couldn't run ninka: " + e);
                } finally {
                    Util.close(p2, p1, fout);
                    tmp.delete();
                }
            }

            lic = lic != null ? lic.trim() : "";
            lic = lic.replace('\t', ' ');
            if (lic.startsWith("/var/tmp/jarchive/thread_" + threadId + "/")) {
                lic = lic.substring(("/var/tmp/jarchive/thread_" + threadId + "/").length());
            }
            if (lic.startsWith("/dev/shm/jarchive/thread_" + threadId + "/")) {
                lic = lic.substring(("/dev/shm/jarchive/thread_" + threadId + "/").length());
            }            
            String fullLic = lic;
            int x = lic.indexOf(';');
            String licName = "";
            if (x >= 0) {
                licName = lic.substring(0, x);
                int y = lic.indexOf(';', x + 1);
                if (y >= 0) {
                    lic = lic.substring(x + 1, y);
                } else {
                    lic = lic.substring(x + 1);
                }
            }
            lic = lic.trim();
            if (!licName.equals(name + ".java")) {
                lic = "";
            }
            if (lic.length() > 254) {
                lic = lic.substring(0, 254);
            }
            if (fullLic.length() > 254) {
                fullLic = fullLic.substring(0, 254);
            }
            long delay = System.currentTimeMillis() - now;

            StringBuilder buf = new StringBuilder(256);
            if (a instanceof Src) {
                buf.append("INSERT INTO java_raw VALUES (");
            } else {
                buf.append("INSERT INTO class_raw VALUES (");
            }
            buf.append("'").append(getSHA1ClassSig(a, true)).append("', ");
            buf.append("'").append(getSHA1ClassSig(a, false)).append("', ");
            buf.append("'").append(pkg).append("', ");
            buf.append("'").append(name).append("', ");
            buf.append("'").append(ver).append("', ");
            buf.append("'").append(DF.format(d)).append("', ");
            buf.append("'").append(folder).append("', ");
            buf.append("'").append(path).append("', ");
            buf.append(methodCount).append(", ");
            buf.append(size).append(", ");
            buf.append(minLine).append(", ");
            buf.append(maxLine);
            if (a instanceof Src) {
                buf.append(", '").append(lic).append("'");
                buf.append(", '").append(fullLic).append("'");
            }
            buf.append(");");
            if (a instanceof Src) {
                SRC_SQL.println(buf);
            } else {
                BIN_SQL.println(buf);
            }
        } catch (Exception e) {
            System.out.println("problem doing insert: " + e + " " + a.getPath());
            e.printStackTrace(System.out);
        } finally {
            IN_FLIGHT.remove(a);            
        }
    }

    /*
  sig                           bytea         NOT NULL,
  pkg                           varchar(255)  NOT NULL,
  name                          varchar(255)  NOT NULL,
  version                       varchar(255)  NOT NULL,
  date            timestamp without time zone NOT NULL,

  folder                        varchar(255)  NOT NULL,
  path                          varchar(4096) NOT NULL,
  methodCount                   int           NOT NULL,
  size                          bigint        NOT NULL,
  minLine                       int           NOT NULL,
  maxLine                       int           NOT NULL,
  lic                           varchar(255)  NOT NULL
     */

    private static class NinkaJob implements Runnable {

        private final Artifact a;

        public NinkaJob(Artifact a) {
            this.a = a;
        }

        public void run() {
            Thread t = Thread.currentThread();
            Integer id = THREAD_IDS.get(t);
            if (id == null) {
                synchronized (THREAD_IDS) {
                    id = THREAD_IDS.get(t);
                    if (id == null) {
                        id = THREAD_IDS.size();
                        THREAD_IDS.put(t, id);
                    }
                }
            }

            insertFromThread(a, id);
        }

    }
}


