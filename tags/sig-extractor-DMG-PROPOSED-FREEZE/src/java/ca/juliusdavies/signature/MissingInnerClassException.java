package ca.juliusdavies.signature;

public class MissingInnerClassException extends RuntimeException {
    public final String fqn;

    public MissingInnerClassException(String fqn) {
        this.fqn = fqn;
    }
}
