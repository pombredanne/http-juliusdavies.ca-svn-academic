package ca.juliusdavies.signature;

public class ScanConfig {
    public final static ScanConfig DEFAULT_CONFIG = new ScanConfig();

    public final boolean sortInnerClasses;
    public final boolean sortMethods;
    public final boolean sortFields;

    public ScanConfig() { this(false, false, false); }

    public ScanConfig(boolean si, boolean sm, boolean sf) {
        this.sortInnerClasses = si;
        this.sortMethods = sm;
        this.sortFields = sf;
    }
}
