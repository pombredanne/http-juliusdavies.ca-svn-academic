package ca.juliusdavies.signature;

public class Names {


    public static String extractNameFromPath(String path) {
        int x = Math.max(path.lastIndexOf('/'), path.lastIndexOf('\\'));
        return x >= 0 ? path.substring(x+1) : path;
    }

    public static String fixClassName(String className) {
        return className.replace('/', '.').replace('$', '.');
    }


    public static String stripGeneric(String name) {
        int x = name.indexOf('<');
        return x >= 0 ? name.substring(0,x) : name;
    }

    public static String fixTypeName(String typeName) {

        String s = fixClassName(typeName);
        if (s.indexOf('.') < 0) { return typeName; }

        StringBuilder buf = new StringBuilder(s);
        s = buf.reverse().toString();
        buf = new StringBuilder(typeName.length());

        boolean keepChars = true;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (c) {
                case '.':
                    keepChars = false;
                    break;
                case ' ':
                case '<':
                case '>':
                case ',':
                case ']':
                case '[':
                    keepChars = true;
                    break;
            }

            if (keepChars) {
                buf.append(c);
            }
        }


        return buf.reverse().toString();
    }


}
