package ca.juliusdavies.signature;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * @author Julius Davies
 * @since Mar 28, 2010
 */
public abstract class Unpacker {

    private static String SOURCE_DIR = ".";
    private static String TARGET_DIR = ".";
    public static boolean INCLUDE_ZIPS = true;

    /*

   .zip
   .jar
   .tar.bz2
   .tar.gz
   .tgz
   .jar.pack.gz

    */

    public static void main(String[] args) throws IOException {
        if (args.length > 0) {
            TARGET_DIR = args[0];
        }
        if (args.length > 1) {
            SOURCE_DIR = args[0];
            TARGET_DIR = args[1];
        }
        SOURCE_DIR = new File(SOURCE_DIR).getCanonicalPath();
        TARGET_DIR = new File(TARGET_DIR).getCanonicalPath();
        if (SOURCE_DIR.equals(TARGET_DIR)) {
            System.err.println("Source and target must be different: [" + SOURCE_DIR + "]");
            System.exit(1);
        }
        scan(new File(SOURCE_DIR));
    }

    private static void scan(File d) throws IOException {
        if (d.isDirectory()) {
            for (File f : d.listFiles()) {
                scan(f);
            }
        } else if (d.isFile() && d.canRead() && d.length() > 0) {
            extract(d);
        }
    }

    static void extract(File f) throws IOException {
        String name = f.getName().toLowerCase(Locale.ENGLISH);
        boolean isTar
                = name.endsWith(".tar.gz")
                || name.endsWith(".tgz")
                || name.endsWith(".tar.bz2")
                || name.endsWith(".tar")
                || name.endsWith(".jar.pack")
                || name.endsWith(".jar.pack.gz")
                || (INCLUDE_ZIPS && name.endsWith(".zip"))
                || (INCLUDE_ZIPS && name.endsWith(".jar"));

        if (!isTar) {
            return;
        }

        String dir = f.getParentFile().getCanonicalPath();
        String t;
        File T;
        if (dir.startsWith(SOURCE_DIR)) {
            t = dir.replace(SOURCE_DIR, TARGET_DIR);
            T = new File(t + "/" + f.getName());
            if (!T.exists()) {
                T.mkdirs();
            }
            t = T.getCanonicalPath();
        } else {
            throw new RuntimeException("sourceDir not present: [" + dir + "] vs [" + SOURCE_DIR + "]");
        }

        String archive = f.getCanonicalPath();
        ArrayList<String> cmd = new ArrayList<String>();
        cmd.add("/bin/bash");
        cmd.add("-c");
        if (name.endsWith(".tar.bz2")) {
            cmd.add("bunzip2 -c " + archive + " | tar -x -C " + t);
        } else if (name.endsWith(".tar.gz") || name.endsWith(".tgz")) {
            cmd.add("gunzip -c " + archive + " | tar -x -C " + t);
        } else if (name.endsWith(".tar")) {
            cmd.add("tar -x -C " + t + " -f " + archive);
        } else if (name.endsWith("jar.pack.gz") || name.endsWith("jar.pack")) {
            int charsAfterJar = name.endsWith("jar.pack.gz") ? 8 : 5;
            int endPos = archive.length() - charsAfterJar;

            cmd.add("unpack200 " + archive + " " + archive.substring(0, endPos));
            exec(cmd);

            File jar = new File(archive.substring(0, archive.length() - 8));
            unzip(new ZipFile(jar), t);
            jar.delete();
            return;
        } else if (name.endsWith(".zip") || name.endsWith(".jar")) {
            unzip(new ZipFile(f), t);
            return;
        } else {
            return;
        }

        exec(cmd);

    }

    private static void exec(ArrayList<String> cmd) throws IOException {
        System.out.print(cmd.get(cmd.size() - 1));
        String[] exec = cmd.toArray(new String[cmd.size()]);
        Process p = null;
        try {
            Thread.sleep(1);
            p = Runtime.getRuntime().exec(exec);
            InputStream in = p.getInputStream();
            OutputStream out = p.getOutputStream();
            try {
                out.close();
                in.close();
            } catch (Exception e) {
                System.out.println("!!! " + e);
            } finally {
                System.out.println(" exec(): " + p.waitFor());
            }
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        } finally {
            if (p != null) { p.destroy(); }
        }

    }

    public static void unzip(ZipFile zf, String target) throws IOException {
        Enumeration<? extends ZipEntry> entries = zf.entries();
        while (entries.hasMoreElements()) {
            ZipEntry ze = entries.nextElement();
            File f = new File(target, ze.getName());
            f.getParentFile().mkdirs();
            if (!ze.isDirectory()) {
                InputStream in;
                FileOutputStream fout;
                in = zf.getInputStream(ze);
                fout = new FileOutputStream(f);

                try {
                    byte[] buf = new byte[4096];
                    int c = in.read(buf);
                    while (c >= 0) {
                        if (c > 0) {
                            fout.write(buf, 0, c);
                        }
                        c = in.read(buf);
                    }
                } finally {
                    fout.close();
                    in.close();
                }

            }
        }
        System.out.println("Unzipped " + zf.getName() + " to " + target);
    }
}
