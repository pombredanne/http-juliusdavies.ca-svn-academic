package ca.juliusdavies.signature;

import ca.phonelist.util.Bytes;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


public class ZipUtil {

    public static Map<NameAndBytes, List<NameAndBytes>> extractClasses(final String zipPath, final ZipFile zf) {
        TreeMap<String, NameAndBytes> pathToNab = new TreeMap<String, NameAndBytes>();
        TreeMap<NameAndBytes, List<NameAndBytes>> innerClasses = new TreeMap<NameAndBytes, List<NameAndBytes>>();

        // 1st pass... get all the parent classes done.
        Enumeration<? extends ZipEntry> en = zf.entries();
        while (en.hasMoreElements()) {
            final ZipEntry ze = en.nextElement();
            final String path = ze.getName();

            // Must be parent class:
            if (path.endsWith(".class") && !InnerClass.isChildClass(path)) {

                // Extract can fail (IOException).
                NameAndBytes nab = extract(zipPath, zf, ze);
                if (nab != null) {
                    innerClasses.put(nab, new ArrayList<NameAndBytes>());
                    if (pathToNab.put(path, nab) != null) {
                        System.err.println("WARN: " + zipPath + "/!/" + path + " already scanned!");
                    }
                }
            }
        }

        // 2nd pass... inner-classes.
        en = zf.entries();
        while (en.hasMoreElements()) {
            final ZipEntry ze = en.nextElement();
            final String path = ze.getName();
            if (InnerClass.isChildClass(path)) {

                // It's an inner-class, and we know its parent.
                final String parent = InnerClass.ultimateParent(path);

                // Associate it with its parent.
                NameAndBytes parentNab = pathToNab.get(parent);
                List<NameAndBytes> childClasses = innerClasses.get(parentNab);
                if (childClasses != null) {

                    // Extract can fail (IOException).
                    NameAndBytes nab = extract(zipPath, zf, ze);
                    if (nab != null) {
                        childClasses.add(nab);
                    }

                } else {
                    System.err.println("WARN: " + zipPath + "/!/" + path + " inner-class with no parent!");
                }
            }
        }

        return innerClasses;
    }

    private static NameAndBytes extract(String zipPath, ZipFile zf, ZipEntry ze) {
        String path = ze.getName();
        String fullPath = zipPath + "/!/" + path;
        try {
            String name = Names.extractNameFromPath(path);
            InputStream in = zf.getInputStream(zf.getEntry(path));
            byte[] bytes = Bytes.streamToBytes(in);
            return new NameAndBytes(
                    NameAndBytes.BINARY, name, fullPath, ze.getTime(), bytes
            );
        } catch (IOException ioe) {
            System.err.println(fullPath + " " + ioe);
            return null;
        }
    }

    public static void main(String[] args) throws IOException {


        ZipFile zf = new ZipFile(args[0]);
        File f = new File(args[0]);

        Map<NameAndBytes, List<NameAndBytes>> m = ZipUtil.extractClasses(f.getCanonicalPath(), zf);
        for (Map.Entry<NameAndBytes, List<NameAndBytes>> entry : m.entrySet()) {
            NameAndBytes key = entry.getKey();
            List<NameAndBytes> val = entry.getValue();

            Bin bin = new Bin(key, val, ScanConfig.DEFAULT_CONFIG);
            System.out.println("----------------------------");
            System.out.println(bin.getPath());
            System.out.println("---");
            System.out.println(bin.toTightString());
        }

    }


}