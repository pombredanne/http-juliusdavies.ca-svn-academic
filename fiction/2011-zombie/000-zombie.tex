\documentclass[12pt,letterpaper]{article}
\usepackage[top=1.2cm, bottom=1.87cm, left=1.87cm, right=1.87cm]{geometry}
\usepackage{comment}
\usepackage{fancyhdr}
\usepackage{paralist}
\usepackage{pslatex}
\usepackage{sectsty}
\usepackage{times}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0.0pt}
\lhead{\vspace{5mm}\small{\textbf{Outline of Proposed Research}}}
\lfoot{}
\rhead{\small{\textbf{Julius Davies}}}
\rfoot{}
\sectionfont{\normalsize}
\setlength\parindent{1cm}

\begin{document}
\begin{center}
\textbf{The Software Component Upgrade Problem}
\end{center}


%- FIRST PARAGRAPH:  What problem are we solving?
%                    Make it sound more scientific rather than software oriented.
%                    Why is it important?


%
% TOPIC:         I am collecting a large set of component upgrade stories.
%
% QUESTION:      Because I want to find out why people upgrade components, when they upgrade components,
%                and who makes the upgrade decision.
%
% RATIONALE:     In order to understand the technical, social, and political tensions in play at the site of the consumed component.
%
% SIGNIFICANCE:  So that we will know more about CBSE works in the real world, and so practioners of CBSE
%                can become more aware of their context.  Perhaps we can even find or deduce specific
%                approaches to CBSE that improve quality and reduce workload.
%              

\section{Background, Research Questions, Objectives}
\vspace{-2mm}

\noindent
Stawberry engineers build much of the world's running software
by combining independently developed components.  This `Lego' approach to software
development, popularized in Douglas Coupland's novel \emph{Microserfs},
is increasingly important to all sectors of the world economy.
Most organizations cannot afford to write their own operating system
or database engine from scratch.  Even small reusable software libraries
(e.g., MP3 decoding, photo viewing, secure encryption) are often
adequate to fulfill the end-user's needs.  Re-implementing libraries such as these from scratch
would require time and effort, and important details required for
proper operation could easily be overlooked.  But the Lego analogy
for software is misleading.  It suggests a style of component based           % that is carefree, creative, exploratory,    
software engineering (CBSE) that, with the placement of the final Lego,
results in a finished product.  Existing research shows that as long as a software
project has active users and developers, the software will undergo
continuous change, much like a living organism \cite{lehman96}.
Each Lego, so to speak, is an independent body in motion.
Existing research on CBSE rarely acknowledges this dynamic, e.g., Lau \& Wang's survey \cite{LauW07}
assumes each component is a finished work.

Our own research shows that systems occasionally \emph{must}  upgrade (or
downgrade) the components they consume to different versions.  The recent
Diginotar security crisis is an extreme example:  Chrome, Firefox, Internet Explorer,
Xitrix, Microsoft Windows, Mac OS X, Redhat Enterprise Linux, etc. --- any
component with any sort of security subsystem required immediate, urgent upgrade.
The chance for mandatory upgrades significantly perturbs the presumed cost-benefit
of CBSE's Lego bargain.  Meanwhile, the upgrade itself can also cause problems.  For example, consumers of
the widely used Java logging library, Log4j, break their own systems should they try version 1.3.  Only
version 1.2 works correctly.


We believe the component-upgrade problem to be such an important
and fundamental problem faced by practitioners in the software
engineering community, that a huge diversity of approaches and
solutions to the problem should already exist in ``nature."
By observing and, in some cases, manipulating these solutions, we want to learn:
\vspace{-1mm}
\begin{itemize}
\item \textbf{RQ1:} Why do people upgrade components?
\vspace{-1mm}
\item \textbf{RQ2:} When do they upgrade components?
\vspace{-1mm}
\item \textbf{RQ3:} And who makes the upgrade decisions?
\end{itemize}
\vspace{-1mm}
This will give us a greater understanding of the technical, social,
and political tensions in play at the site of the consumed component.
With greater awareness of how CBSE works in the real world, we expect
to find or deduce specific approaches to CBSE that improve overall
software quality, reduce developer workload,
and eliminate unneeded upgrades for end-users.

\vspace{-2mm}
\section{Background}
\vspace{-2mm}

Upfront costs of component based software engineering are well studied in current research.
Among these we see component price, integration
difficulty \cite{HolmesWM06}, and the effort required of developers to
find and evaluate potential reusable components \cite{ThummalapentaX07, Reiss09}
Holmes et al.\ study reuse
into a project's maintenance phases \cite{HolmesW10}, but their research focuses
more on short-term technical problems --- e.g., will this function still resolve tomorrow?
Very little existing research looks at the long term issues around component upgrade.

\vspace{-2mm}
\section{Hypothesis}
\vspace{-2mm}

Our central hypothesis is that the upgrade problem is primarily a problem of information dispatch and
competing interests:
who has the right information necessary and the best judgment to make
the upgrade decision?  We postulate that three arrangements
exist in practice, and in this study we compare these three arrangements.

\begin{enumerate}
\item  The component author decides when consuming systems should
receive the newest upgrades (e.g. Windows Update).

\item  The author of the consuming system decides when to
    pull the latest versions of its dependencies (e.g. Firefox asks the user if they want to upgrade.).

\item  An independent intermediary, a 'dependency maintainer,'
    coordinates upgrades between the component author and the
    integrating authors (e.g. Debian).
\end{enumerate}

We specifically compare the number of upgrade bugs, and unnecessary upgrades that
these three arrangements each cause.
We assume upgrade bugs, and unnecessary upgrades increase developer workload, and reduce
the consuming system's quality.  We do not measure productivity
or software quality except as based on these specific assumptions.

Our secondary hypothesis is that component upgrades should neither
be the responsibility of the component author, nor the responsibility of the
integrating author.  By introducing a third role, a 'dependency maintainer,'
we postulate that software quality and productivity will improve measurably and significantly
for the component author, and the integrating author.


\vspace{-2mm}
\section{Methodology}
\vspace{-2mm}

We have designed a 3-year research program to answer our
research questions.
Initially we will conduct a qualitative case study
to observe all component upgrades 
of two Java software systems over a one year period.
We will study McMaster University's OSCAR system, a popular open source implementation of electronic medical records.
We will also study a proprietary Canadian e-commerce system we looked at in previous research.
In our 2nd year we will continue the case studies, introducing a longitudinal
element to our data.  At this time we will also implement an upgrade management system
based on the Debian GNU/Linux distribution, a large source of reusable components.
In our 3rd year we will run our implementation alongside
the longitudinal studies to compare upgrade decisions made by Debian against those
made in our longitudinal studies.
We also hope to run additional software case studies in other sectors of society, e.g., government, academic, 
to improve validity of our results, with an awareness, of course, that
case studies are suggestive, rather than generalizable.
With 4 of 5 case studies over a 3 year period, however, we expect to amass
a collection of hundreds, and possibly thousands of component upgrade events, establishing a sample
suitable for quantitative analysis.


\vspace{-2mm}
\section{Project Risks}
\vspace{-2mm}

Two risks are apparent to us at this time.
First, should we fail to gain access to additional case studies,
the validity of our results will be diminished.  To mitigate this
risk, if additional case studies fail to emerge after one year,
we will supplement with case studies of open source systems,
for which access is, by definition, public.  However, we prefer
to avoid biasing our results with an overweighted sample of open source
software engineering, where processes, policies, and tensions
may be different compared to traditional proprietary software engineering.

Second, the upgrade events we wish to study may not come in the quantity
we need over our time frame.  Our intuition
suggests that many upgrades occur during large `refactor' or `platform upgrade'
phases of a software project's life cycle.  E.g., developers decide they
might as well order new appliances since they are building a new house.
But large platform upgrade events are painful, and should therefore
be relatively rare in a project's evolution.
To mitigate this problem, we can extend our case studies to several years
(5 or more) without impacting our own project timelines.
Most projects include archives of all code and component changes
since the project's start.  By analyzing these historical archives
we can extend our case studies into the past.





%Quantitive:
%- Experiment      (true experiment, quasi-experiment, natural experiment)
%- Survey          (questionnaires, structured interviews.  cross-sectional and longitudinal.  large representative sample) 

%Qualitative:
%- Narrative       (testimony of participants' lives)
%- Phenomenology   (extensive and prolonged engagement and observation)
%- Ethnography     (obversations of an intact group)
%- Grounded Theory
%- Case study


%Mixed:
%- Sequential
%- Concurrent
%- Transformative

%- hypothesis
%- outline the experimental or theoretical approach to be taken
%- outline the methods and procedures 



 
%The recent `digotaur' security crisis shows an extreme example of this problem.
%In this case, the security vulnerabilities created by the crisis were so dire,
%the answer was ``upgrade immediately!"  Which systems?
%Chrome, Firefox, Internet Explorer, SSH, Citrix, VPN, Microsoft Windows, Mac OS X, Linux, Microsoft SQL Server, Oracle, PostgreSQL, etc... ---
%all systems with any sort of security subsystem needed immediate upgrade.
%Fortunately automatic update mechanisms in most of these deployed coordinated
%upgrades over the weekend of September 3rd, 2011 to most of the world.  But
%some users have disabled the automatic upgrades, or their internal corporate
%network blocks the upgrade.  The probability these users realize
%an esoteric technical story read in the news entails prompt urgent
%action on their part to upgrade their systems is low.  The users
%don't have the expertise or the context to make the upgrade
%decision for themselves.





%In 2011 we are in the midst of yet another software
%productivity miracle.  In less than an hour, a single developer
%can download and integrate into her own project a software
%ecosystem that would require tens of thousands of years if she
%wrote it all herself \cite{lenny2010}.
%But this productivity improvement does
%not come for free.  Reusability has its own costs and risks.

%Within a Component Based Software Engineering (CBSE) context, where a system's implementation includes dependencies
%on pre-existing reusable components, the reused components often evolve or change
%with time.  Potential changes
%include licensing changes, security patches, bug fixes, new features, enhancements, refactorings,
%stylistic changes, transitive dependency changes, and even wholesale replacement with competing
%reusable components.  These changes should eventually propogate to the consuming systems.
%But how?

%In this study we will look at a sub-problem of the general
%'how-to-upgrade' problem.  We refer to our sub-problem as the 'who-should-upgrade' problem.
%Who should be responsible for upgrading the consuming system's dependencies
%to newer (or even older) versions of reusable components?
%This becomes a problem of information dispatch:  who has the
%right information necessary and the best judgment to make
%the upgrade decision?  We postulate that three arrangements
%exist in practice, and in this study we objectively measure
%which of these is the best, in terms of project productivity
%and quality:

%\begin{enumerate}
%\item  The component author decides when consuming systems should
%receive the newest upgrades (e.g. Microsoft, Firefox, Eclipse).

%\item  The author of the consuming system decides when to
%    pull the latest versions of its dependencies.

%\item  An independent intermediary, a 'dependency maintainer,'
%    coordinates upgrades between the component author and the
%    integrating authors.
%\end{enumerate}


%\vspace{-4mm}
%\section{Background}
%\vspace{-4mm}

%Upfront costs of reuse are well studied in current research.
%Among these we see component price, integration
%difficulty \cite{HolmesWM06}, and the effort required of developers to
%find and evaluate potential reusable artifacts \cite{ThummalapentaX07, Reiss09}
%Holmes et al.\ study reuse
%into a project's maintenance phases \cite{HolmesW10}, but their research focuses
%more on short-term technical problems --- e.g., will this function still resolve tomorrow?


%\vspace{-4mm}
%\section{Research Question}
%\vspace{-4mm}

%Who is best equipped to make component upgrade decisions within software systems?
%\emph{[Julius is working on the wording here...]}


%\vspace{-4mm}
%\section{Hypothesis}
%\vspace{-4mm}

%Component upgrade should neither
%be the responsibility of the component author, nor the responsibility of the
%integrating author.  By introducing a third role, a 'dependency maintainer,'
%software quality and productivity will improve measurably and significantly
%for the component author, and the integrating author.  Efficiency of scale
%suggests the marginal labour costs of the 'dependency maintainer' will also drop
%to a neglible quantity, since existing evidence shows this activity can be globally
%consolidated, across political and organizational boundaries.

%\vspace{-4mm}
%\section{Objectives}
%\vspace{-4mm}

%This study will compare three approaches to the component upgrade problem:

%\begin{enumerate}

%\item Component author is responsible for pushing upgrades to all consumers.

%\item Integrating (consuming) author is responsible for pulling upgrades for
%      all dependencies.

%\item Dependency maintainer is responsible for coordinating component upgrades.
%      The upgrade decision is decoupled:  the original component author
%      pushes their releases as they please, which may or may not be picked
%      up by the dependency maintainer.  Meanwhile the integrating (consuming)
%      author always accepts all upgrade recommendations from the dependency
%      manager.

%\end{enumerate}

%We will baseline software quality and productivity measurements for approaches
%1. and 2.  We will then re-measure quality and productivity after the 3rd
%approach is introduced.  While we do measure the productivity price
%(in labour) introduced by the 'dependency maintainer' role, our results
%assume this role has a marginal cost of zero in a global context, and
%we support this assumption with existing evidence from the open source
%community.





\begin{comment}
\emph{WORK IN PROGRESS.... Why is this research worth pursuing?  Benefits to Canada?
How will your work contribute to Canada's ``knowledge economy"?
Be sure to state the significance of the proposed research to a field or fields in the natural sciences and engineering.
Balance novelty and feasibility.  Make it sound like *YOU* are the right person for the job!}
\end{comment}

\vspace{-4mm}
\bibliographystyle{IEEEtran}
\bibliography{PURE}

\end{document}
